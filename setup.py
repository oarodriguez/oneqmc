"""
OneQMC
======

OneQMC is a python library to perform Quantum Monte Carlo simulations
on many-body quantum systems.

OneQMC implements the Variational Monte Carlo (VMC) approach, and it uses
the Metropolis-Hastings algorithm to evaluate an approximated value of
several properties of a physical system as the ground state energy and
the structure factor, among other properties.

The library is written in pure python, as it uses
`numba <http://numba.pydata.org/>`_ to just-in-time compile
performance-critical functions that execute numerical CPU-intensive
calculations. Also it is BSD licensed.
"""
from setuptools import setup

NAME = 'oneqmc'

VERSION = '0.1dev1'


setup(
    name=NAME,
    version=VERSION,
    url='https://bitbucket.org/oarodriguez/oneqmc',
    license='BSD-2',
    author='Abel Rodríguez',
    author_email='oarodriguez@outlook.com',
    description='A library to perform Quantum Monte Carlo simulations on '
                'many-body quantum systems.',
    long_description=__doc__,
    packages=[
        'oneqmc',
        'oneqmc.cli',
        'oneqmc.cli.multirods',
        'oneqmc.cli.multislabs',
        'oneqmc.multirods',
        'oneqmc.multirods.models'
    ],
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    install_requires=[
        'numpy>=0.10',
        'scipy>=0.17.0',
        'numba>=0.25',
        'h5py>=2.5.0',
        'pandas>=0.18',
        'mpmath>=0.19',
        'click>=6.6',
        'progressbar2>=3.6.0',
        'pytz>=2016.4',
        'tzlocal>=1.2'
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering :: Physics',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
