import os
import sys
from abc import ABCMeta, abstractmethod
from collections import Iterator, Sequence, Mapping, OrderedDict, \
    MutableMapping, Iterable
from copy import deepcopy
from enum import IntEnum
from functools import reduce
from operator import mul, add

import numpy as np
from h5py import File

from oneqmc.config import ConfigSet, getitem_, setitem_
from oneqmc.defaults import META_KEY, normalize_export_meta, \
    normalize_export_metropolis
from oneqmc.types import items_dotted


def make_grid_spec(config, grid_config):
    """Builds a sequence of pairs ``(key, array)`` from the configuration
    set ``config``. The function looks for two keys in the ``grid_config``
    mapping: ``major`` and ``minor``, the second being optional. The value
    of these items must be an iterable of strings, where each element will
    refer to a key in the ``config`` object that will be one of the grid
    arrays.

    This function does not make any type checking, but it expects that the
    elements in ``config`` being referred by ``grid_config`` are ``numpy``
    arrays. In addition, it accepts keys in dotted notation to access sub
    items in ``config``.

    :param config: A mapping object which contains the grid arrays.
    :param grid_config: A mapping object with the keys referring to some
                        range array in ``config``.
    :return: A tuple with the sequence of ``(key, array)`` for the major
             grid and the minor grid (this last may be None as it is
             optional)
    """
    major_config = grid_config['major_grid']
    minor_config = grid_config.get('minor_grid', None)

    # TODO: Some type checking may be good (maybe turing this into a class)
    major_spec = [(k, getitem_(config, k)) for k in major_config]
    if minor_config is not None:
        minor_spec = [(k, getitem_(config, k)) for k in minor_config]
        return major_spec, minor_spec

    return major_spec, None


def make_grid(ranges_specs):
    """Makes a dense grid with the arrays within the ``ranges_specs``
    sequence.

    :return: A numpy ``ndarray`` object representing the grid.
    """

    def times(d1, d2):
        return d1 * d2

    grid = []
    for idx, range_spec in enumerate(ranges_specs):
        _, rng = range_spec
        rng_shape = [1] * len(ranges_specs)
        fvs = reduce(times, rng.shape)
        rng_shape[idx] = fvs
        rng_copy = rng.copy()
        grid.append(rng_copy.reshape(rng_shape))

    return np.broadcast_arrays(*grid)


def pack_grids(coord_arrays):
    """Stack grids along their last axis.

    :param coord_arrays: A list or coordinated arrays to be stacked.
    :return: The stacked array.
    """
    return np.stack(coord_arrays, axis=-1)


class Estimator(metaclass=ABCMeta):
    """Abstract implementation of an estimator."""

    __exec_config__ = {}

    def __init__(self, config=None):
        self.config = config

    @abstractmethod
    def eval(self, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def blank_config_copy(cls):
        """Makes a deep copy of the estimator basic configuration."""
        return deepcopy(cls.__exec_config__)

    @classmethod
    @abstractmethod
    def is_instance_param(cls, config_param):
        raise NotImplementedError


class GSItemType(IntEnum):
    """Available modes to initialize a  composite :class:`GridSpecItem` 
    instance."""

    #: Indicates that a instance contents should be zipped internally.
    ZIP = 0

    #: Indicates that contents should be broadcast as n-dimensional numpy
    #: ``ndarrays``.
    BROADCAST = 1

    #: TODO: Do we need this type?
    ZIP_BROADCAST = -1


class GridSpecItem(Sequence):
    """Represents a single grid specification item."""

    def __init__(self, names, contents, type_=None):
        """Initialize a :class:`GridSpecItem` instance.
    
        :param names: The name or a list of names to construct the item. It could
                      be a single string or a sequence of strings.
        :param contents: The contents of the item. The supplied value must 
                         follow the following rules:
                         
                         * If ``names`` is a single string then it should be a
                           single numeric sequence or a multi-dimensional
                           ``numpy.ndarray``.
                         * If ``names`` is a sequence of strings then it should
                           be a collection with the same length than``names``. 
                           The contents of this collection should be sequences 
                           or ``numpy.ndarray`` instances.
                           
        :param type_: Indicates how the item should be composed. 
        """
        if not isinstance(names, Sequence):
            raise TypeError("{} must be a string or a sequence of "
                            "strings".format(names))
        if not isinstance(names, (str, bytes)):
            # Names is a ``Sequence`` but not string
            if len(names) != len(contents):
                raise ValueError("incompatible {} and {} sequences")
            names = tuple(names)
            contents = contents
        else:
            # ``names`` is a string, so store as a one-element tuple.
            names = names,
            contents = [np.array(contents)]

        #: A list with the names of the contents.
        self.names = names

        #: Indicates how the contents data is transformed and managed
        #: internally by the item instance.
        self.type = type_ or GSItemType.ZIP

        # Just an internal reference for the original content.
        self._original_contents = contents

        # Zip mode
        if self.type == GSItemType.ZIP:
            # All sequences in ``contents`` container must have the same length.
            self.check_contents_lengths(contents)
            shape = len(contents[0]),
            size = reduce(mul, shape)

        # Broadcast mode
        elif self.type == GSItemType.BROADCAST:
            # The ``contents`` in container must pass a broadcast conversion.
            try:
                contents = [
                    np.array(a) for a in np.broadcast_arrays(*contents)
                ]
            except ValueError:
                # TODO: Maybe raise a better exception
                raise

            shape = contents[0].shape
            size = reduce(mul, shape)

        else:
            raise ValueError("type '{}' is not recognized".format(self.type))

        #:
        self.contents = contents

        # The shape and total size of the item.
        self._shape = shape
        self._size = size

    @property
    def size(self):
        """The size of the item contents."""
        return self._size

    @property
    def shape(self):
        """The shape of the item contents."""
        return self._shape

    @staticmethod
    def check_contents_lengths(contents: Sequence):
        """
        
        :param contents: 
        :return: 
        """
        cl = len(contents[0])
        if len(contents) > 1:
            for seq in contents[1:]:
                if len(seq) != cl:
                    raise KeyError('all elements in container must have '
                                   'the same length')

    def __getitem__(self, item):
        if not isinstance(item, (int, tuple)):
            raise IndexError('{}'.format(item))
        return [cnt[item] for cnt in self.contents]

    def __iter__(self):
        if self.type == GSItemType.ZIP:
            return iter([n, c] for n, c in zip(self.names, self.contents))
        nd_index = np.ndindex(self.shape)
        return iter(self[ndx] for ndx in nd_index)

    def __len__(self):
        return self.shape[0]

    def __repr__(self):
        return '{}({}, {}, type={})'.format(self.__class__.__name__,
                                            self.names,
                                            self.contents,
                                            self.type)


class GridSpec(MutableMapping):
    """Represents a n-dimensional grid used to execute a simulation."""

    def __init__(self, keys, config):
        """A mapping that contains the elements that form the grid
        used to execute a simulation.
        
        :param keys: The names of the elements of the grid.  
        :param config: The configuration dictionary that contains the data
                       for the grid elements.
        """
        # References to the original objects.
        self._keys = keys
        self._config = config

        if not isinstance(keys, Sequence):
            raise TypeError("required object of type 'Sequence'")

        if isinstance(keys, (str, bytes)):
            raise TypeError("'str' or 'bytes' objects are not valid keys "
                            "sequences")

        # The collection of items in the initialization order.
        items = []
        for k in keys:
            item = self.make_item(k, config)
            keys = item.names
            items.append((keys, item))

        #: The inner ordered data.
        self.data = OrderedDict(items)

    @property
    def shape(self):
        return reduce(add, (item.shape for name, item in self.items()))

    @property
    def size(self):
        return reduce(mul, self.shape)

    def make_item(self, obj, config, type_=None):
        """Builds a single :class:`GridSpecItem` attached to the current
         instance.
        
        :param obj: The object that contains the items keys/names. 
        :param config: A mapping object with the values that will go into
                       the item instance.
        :param type_: The mode used to compose the :class:`GridSpecItem`
                      instance if specified. Default ro ``None``.
        :return: A new :class:`GridSpecItem`.
        """
        if isinstance(obj, Sequence):
            if isinstance(obj, (str, bytes)):
                return GridSpecItem(obj, config[obj])
            else:
                contents = [config[k] for k in obj]
                return GridSpecItem(obj, contents, type_)
        elif isinstance(obj, Mapping):
            spec_objects = obj['items']
            type_ = obj.get('composition_type', GSItemType.ZIP)
            if isinstance(type_, str):
                type_ = GSItemType[type_]
            else:
                type_ = GSItemType(type_)
            return self.make_item(spec_objects, config, type_)
        else:
            raise ValueError

    def arrange_index(self, index: tuple):
        """Groups a n-dimensional index (a tuple with ``n`` elements) into
        sub-tuples accordingly to the :class:`GridSpecItem` instances that 
        form the current :class:`GridSpec` instance. The order used to form
        the tuples is the order of the keys used to initialize this grid 
        instance. 
        
        More specifically, given a tuple with :math:`N` elements
        :math:`(n_1, n_2, n_3, \ldots, n_{N-1}, n_N)` and :math:`M` items with
        shapes of size :math:`(s_1, s_2, \ldots, s_{M-1}, s_M)` in the 
        current instance, the indexes are grouped as
        
        :math:`(T_1, T_2, \ldots, T_{M-1}, T_M)`
        
        where :math:`T_k = (\{ k, \ldots, n_{s_1} |  \})`,
         
        :param index: A n-dimensional index, i.e., a tuple with ``n`` 
                         elements. 
        :return: A tuple with ordered items.
        """
        index_ = []
        index = list(index[::-1])
        for item in self.values():
            shape_len = len(item.shape)
            ndx = tuple(index.pop() for _ in range(shape_len))
            index_.append(ndx)
        return index_

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        self.data[key] = value

    def __delitem__(self, key):
        del self.data[key]

    def __iter__(self):
        return iter(self.data)

    def __len__(self):
        return len(self.data)

    def __repr__(self):
        return '{}({},{})'.format(self.__class__.__name__,
                                  self._keys, self._config)


class Grid(Iterator):
    """Iterates over the values of a :class:`GridSpec` instance in a 
    multidimensional fashion, i.e., iterating over each one of the values
    of that grid until exhausted.
    """

    def __init__(self, grid_spec: GridSpec):
        """Initialized s grid iterator object accordingly to the information 
        stored in ``grid_spec``. 

        :param grid_spec: A :class:`GridSpec` instance.
        """
        # TODO: Check length of ranges...
        self.grid_spec = grid_spec
        self.grid_shape = grid_spec.shape
        self.grid_size = reduce(mul, self.grid_shape)

        #: An N-dimensional index iterator. It allow us to iterate over
        #: all the possible index variations of the arrays that form the
        #: grid.
        # NOTICE: This will cause problems with multiprocessing pools.
        # NOTICE: This is fixed in __setstate__ method.
        self.iterable = self.make_iterable()

    def make_iterable(self):
        """Makes a N-dimensional index iterator from the :attr:`grid_shape`
        attribute.

        :return: An N-dimensional index.
        """
        return np.ndindex(self.grid_shape)

    def __next__(self):
        # The current item of the iterable to use.
        ndx = next(self.iterable)

        grid_element = []
        # TODO: Fix how to extract data from items given a nd-index.
        ndx_ = self.grid_spec.arrange_index(ndx)
        values = self.grid_spec.values()

        for idx, grid_item in zip(ndx_, values):
            names = grid_item.names
            values = grid_item[idx]
            for spec in zip(names, values):
                grid_element.append(spec)

        return grid_element

    def __iter__(self):
        return self

    def __getstate__(self):
        return self.grid_spec, self.grid_shape, self.grid_size

    def __setstate__(self, state):
        # We need to build the iterable manually after unpickling,
        # otherwise the process will fail.
        self.grid_spec, self.grid_shape, self.grid_size = state
        self.iterable = self.make_iterable()


def export(filename, data, evaluator_config, meta=None):
    """Export the data from the evaluation of an estimator to an HDF5
    file. The data is exported together with the configuration used by
    the evaluator object.

    The information of an evaluation is saved in a :class:`h5py.Group`
    instance named ``Execution``, and the attributes of the simulation
    are saved as attributes of this group. For the fields inside any
    configuration section of the simulation the attribute is named
    ``SECTION.FIELD``.

    The exported data is enough to rebuild the last state of the
    simulation by importing the file using the :meth:`load` method.

    :param filename: The filename of the hdf5 file where the
                     simulation state will be exported to.
    """
    utf_8 = 'utf-8'

    def utf_8_enc(str_: str):
        return str_.encode(utf_8)

    def utf_8_dec(bytes_: bytes):
        return bytes_.decode(utf_8)

    # StackOverflow http://stackoverflow.com/questions/2158395/flatten-an-irregular-list-of-lists-in-python
    def flatten(l):
        for el in l:
            if isinstance(el, Iterable) and not isinstance(el, (
                str, bytes)):
                yield from flatten(el)
            else:
                yield el

    export_config = ConfigSet(evaluator_config)

    meta_config = export_config[META_KEY]
    estimator_config = export_config['estimator']
    grid_config = estimator_config['grid_elements']

    major_grid_config = grid_config['major_grid']
    minor_grid_config = grid_config.get('minor_grid', None)

    try:
        with File(filename, 'w-') as fp:
            exec_gp = fp.create_group('Execution')

            normalize_export_meta(meta_config)
            normalize_export_metropolis(estimator_config['metropolis'])

            # HDF5 group for major grid data. The data for each array
            # used for the evaluation is saved in an independent data
            # set with the name of the configuration parameter it
            # belongs. For the major grid configuration there is an
            # ``'MajorGridData'`` group where the data sets are grouped,
            # while for the minor grid configuration, if it exists,
            # there is a ``'MinorGridData'``.
            msg_fmt = 'See MajorGridData/{} data set'
            major_grid_gp = exec_gp.create_group('MajorGridData')
            major_grid_config = list(flatten(major_grid_config))
            for k in major_grid_config:
                major_grid_gp[k] = getitem_(estimator_config, k)
                setitem_(estimator_config, k, msg_fmt.format(k))
            grid_config['major_grid'] = '::'.join(major_grid_config)

            if minor_grid_config is not None:
                minor_grid_config = list(flatten([minor_grid_config]))
                # HDF5 group for minor grid data.
                minor_grid_gp = exec_gp.create_group('MinorGridData')
                msg_fmt = 'See MinorGridData/{} data set'
                for k in minor_grid_config:
                    minor_grid_gp[k] = getitem_(estimator_config, k)
                    setitem_(estimator_config, k, msg_fmt.format(k))
                grid_config['minor_grid'] = '::'.join(minor_grid_config)
            else:
                grid_config['minor_grid'] = False

            # Flatten configuration dictionary before exporting.
            export_config_items = items_dotted(export_config)
            flat_export_config = dict(export_config_items)

            # Now we can update the attributes of the simulation.
            exec_gp.attrs.update(flat_export_config)

            # Store the data array.
            exec_gp['EstimatorData'] = data

            fp.flush()

    except OSError:
        os.remove(filename)
        raise
    except:
        print("An exception occurred while exporting data. Deleting file. "
              "{}".format(sys.exc_info()[0]))
        os.remove(filename)
        raise


def load(filename):
    """Export the data from the evaluation of an estimator to an HDF5
    file. The data is exported together with the configuration used by
    the evaluator object.

    The information of an evaluation is saved in a :class:`h5py.Group`
    instance named ``Execution``, and the attributes of the simulation
    are saved as attributes of this group. For the fields inside any
    configuration section of the simulation the attribute is named
    ``SECTION.FIELD``.

    The exported data is enough to rebuild the last state of the
    simulation by importing the file using the :meth:`load` method.

    :param filename: The filename of the hdf5 file where the
                     simulation state will be exported to.
    """
    utf_8 = 'utf-8'

    def utf_8_enc(str_: str):
        return str_.encode(utf_8)

    def utf_8_dec(bytes_: bytes):
        return bytes_.decode(utf_8)

    import_config = {}

    # meta_config = import_config[META_KEY]
    # estimator_config = import_config['estimator']
    # grid_config = import_config['grid_spec']

    # major_grid_config = grid_config['major_grid']
    # minor_grid_config = grid_config.get('minor_grid', None)

    try:
        with File(filename, 'r') as fp:

            exec_gp = fp['Execution']

            for k, v in exec_gp.attrs.items():
                import_config[k] = v

            major_grid_gp = exec_gp['MajorGridData']
            major_grid_config = import_config[
                'estimator.grid_elements.major_grid']
            config = []
            for k in major_grid_config.split('::'):
                config.append(k)
            import_config['estimator.grid_elements.major_grid'] = config

            for k, v in major_grid_gp.items():
                import_config['estimator.' + k] = v.value

            minor_config = import_config['estimator.grid_elements.minor_grid']
            if minor_config:
                minor_grid_gp = exec_gp['MinorGridData']
                minor_grid_config = import_config[
                    'estimator.grid_elements.minor_grid']
                config = []
                for k in minor_grid_config.split('::'):
                    config.append(k)
                import_config['estimator.grid_elements.minor_grid'] = config

                for k, v in minor_grid_gp.items():
                    import_config['estimator.' + k] = v.value

            # Store the data array.
            estimator_data = exec_gp['EstimatorData'].value

            fp.flush()

    except (AttributeError, KeyError, TypeError, OSError):
        # os.remove(filename)
        raise

    return import_config, estimator_data
