import importlib
import json
import re
from collections import Mapping, MutableMapping, Sequence

import numpy as np

from oneqmc.types import DeepFixedDict, FixedDict

KS = KEY_SEPARATOR = '.'


def getitem_(mapping, key):
    """

    :param mapping:
    :param key:
    :return:
    """
    keys = key.rstrip(KS).split(KS)
    mapping_ = mapping
    for key_ in keys:
        mapping_ = mapping_[key_]
    return mapping_


def setitem_(mapping, key, value):
    """

    :param mapping:
    :param key:
    :param value:
    """
    keys = key.rstrip(KS).split(KS)
    mapping_ = mapping
    for key_ in keys[:-1]:
        mapping_ = mapping_[key_]
    mapping_[keys[-1]] = value


def contains_(mapping, key):
    keys = key.rstrip(KS).split(KS)
    mapping_ = mapping
    for key_ in keys:
        if key_ not in mapping_:
            return False
        mapping_ = mapping_[key_]
    return True


transform_types = dict(__arange__=np.arange, __linspace__=np.linspace,
                       __logspace__=np.logspace, __range__=range)


class ConfigSet(MutableMapping):
    """Represents a set of names and values that form a configuration
    dictionary of type :class:`DeepFixedDict` (or :class:`FixedDict`).
    Its structure is fixed by the arguments supplied during instantiation.
    The choice between :class:`FixedDict` or :class:`DeepFixedDict` is
    fixed by the :attr:`__deep__` attribute.

    :param defaults: A mapping whose values are used to construct the items
                     (keys and values) of the configuration dictionary.
    :param name_: A name for the set. If omitted it is built from
                 the :attr:`__cfg_name__` attribute.
    :param kwargs: Keyword arguments to pass to the constructor of
                   the configuration dictionary.
    """
    __slots__ = ['config']

    #: Indicates the group where the configuration attributes of these
    #: type will refer to.
    __cfg_name__ = None

    #: Whether to instantiate a :class:`DeepFixedDict` or a
    #: :class:`FixedDict` to build the :attr:`config` dictionary.
    __cfg_deep__ = True

    def __init__(self, defaults=None, **kwargs):
        super(ConfigSet, self).__init__()

        if not defaults and not kwargs:
            raise ValueError('non empty initial map or sequence is required')

        # Replace the configuration dictionary for other created
        # directly from the supplied ``defaults`` argument.
        defaults = defaults or ()

        #:
        self.config = self.make_config(defaults, **kwargs)

    @classmethod
    def make_config(cls, defaults, **kwargs):
        """Builds the config dict.

        :param defaults: The default values to instantiate the configuration.
        :return: The configuration object.
        """
        config_type = FixedDict if not cls.__cfg_deep__ else DeepFixedDict
        config = config_type(defaults, **kwargs)
        # Update the values.
        # config.update(*other, **kwargs)
        return config

    @classmethod
    def from_module(cls, module):
        """Fill the configuration with attributes from a python module.
        Only the attributes with names in uppercase are used. Returns a
        new instance of :class:`Set`.

        :param module: The name of the module.
        """
        obj = cls.read_module(module)
        conf_set = cls(defaults=obj)
        conf_set.transform_config()
        return conf_set

    @classmethod
    def from_json_file(cls, file):
        """Reads configuration data from a JSON formatted file and
        returns a new :class:`Set` instance.

        :param file: The path to the file.
        """
        # The JSON data
        with open(file, 'r') as file:
            obj = json.load(file)
        conf_set = cls(defaults=obj)
        conf_set.transform_config()
        return conf_set

    @classmethod
    def from_json(cls, json_str):
        """Reads configuration data from a string written in JSON format
        and returns a new :class:`Set` instance.

        :param json_str: The json string.
        """
        obj = json.loads(json_str)
        conf_set = cls(defaults=obj)
        conf_set.transform_config()
        return conf_set

    def from_object(self, obj):
        """Updates the current instance with the data from a python
        dictionary with configuration data.

        :param obj: The data used to get the configuration.
        """
        self.config.update(obj)
        self.transform_config()

    def load_module(self, module):
        """Updates the current instance with the data from a python
        dictionary with configuration data.

        :param obj: The data used to get the configuration.
        """
        obj = self.read_module(module)
        self.from_object(obj)

    def load_json_file(self, file):
        """Reads configuration data from a JSON formatted file  and stores
        the data in the instance.

        :param file: The path to the file.
        """
        # The JSON data
        with open(file, 'r') as file:
            obj = json.load(file)
        self.from_object(obj)

    def load_json(self, json_str):
        """Reads configuration data from a string written in JSON format
        and stores the data in the instance.

        :param json_str: The json string.
        """
        obj = json.loads(json_str)
        self.from_object(obj)

    @classmethod
    def read_module(cls, module):
        """"""
        obj = {}
        py_module = importlib.import_module(module)
        for name in dir(py_module):
            if name.isupper() and not name.startswith('_'):
                obj[name.lower()] = getattr(py_module, name)
        return obj

    def transform_config(self):
        """"""
        config = self.config
        for k, v in config.items():
            tsv = self.transform_config_item(v)
            config.update([(k, tsv)])

    @classmethod
    def apply_method(cls, method, args, options=None):
        """

        :param method:
        :param args:
        :param options:
        :return:
        """
        options = options or {}
        type_ = transform_types.get(method, None)
        if type_ is None:
            raise ValueError("constructor method '{}' not "
                             "recognized".format(method))
        return type_(*args, **options)

    @classmethod
    def transform_list(cls, item):
        """

        :param item:
        :return:
        """
        if not item:
            return

        method_name = item[0]
        if isinstance(method_name, str) and re.match(r'__(.+)__', method_name):
            args = item[1]
            if len(item) < 3:
                obj = cls.apply_method(method_name, args)
            else:
                obj = cls.apply_method(method_name, args, options=item[2])
            return obj

        obj = []
        for v in item:
            obj.append(cls.transform_config_item(v))
        return obj

    @classmethod
    def transform_config_item(cls, obj):
        """Converts a configuration item to a python object following
        the specification...

        :param obj: The configuration object where the items are stored in.
        :return: The transformed item.
        """
        # TODO: Use Sequence instead?
        if isinstance(obj, (list, tuple)):
            obj = cls.transform_list(obj)
            return obj

        if isinstance(obj, Mapping):
            tns_obj = {}
            for k, v in obj.items():
                tns_v = cls.transform_config_item(v)
                tns_obj[k] = tns_v

            # Return a new mapping with the same type as ``obj``.
            return type(obj)(tns_obj)

        # Just return the item.
        return obj

    def keys(self):
        return self.config.keys()

    def items(self):
        return self.config.items()

    def values(self):
        return self.config.values()

    def update(self, *args, **kwargs):
        self.config.update(*args, **kwargs)

    def __eq__(self, other):
        return self.config.__eq__(other)

    def __getitem__(self, key):
        """If ``key`` is a sequence (a tuple) this method returns a
        tuple with the corresponding elements in the same order.

        :param key: The key of the element.
        :return: The element or collection of elements corresponding to
                 ``key``.
        """
        if isinstance(key, Sequence) and not isinstance(key, str):
            return tuple(getitem_(self.config, sqk) for sqk in key)
        return getitem_(self.config, key)

    def __setitem__(self, key, value):
        """If ``key`` is a sequence (a tuple) this method updates the
        corresponding elements.

        :param key: The key of the element.
        :param value: The values that will update the mapping.
        """
        if isinstance(key, Sequence) and not isinstance(key, str):
            if isinstance(value, Sequence):
                for idx, sqk in enumerate(key):
                    setitem_(self.config, sqk, value[idx])
            else:
                for sqk in key:
                    setitem_(self.config, sqk, value)
            return
        setitem_(self.config, key, value)

    def __delitem__(self, key):
        del self.config[key]

    def __contains__(self, key):
        return contains_(self.config, key)

    def __len__(self):
        return len(self.config)

    def __iter__(self):
        return self.config

    def __repr__(self):
        # TODO: Is this OK? :\
        cls = self.__class__
        return '{}({config})'.format(cls.__name__, config=self.config)
