import math
from abc import abstractmethod
from multiprocessing import current_process
from time import time

import numpy as np
import numpy.random as random
from numba import jit

from oneqmc.base import MonteCarloQuadrature
from oneqmc.types import MonteCarloResult

MAIN_SLOT = 0
AUX_SLOT = 1

UNIFORM_MOVE = 0
GRADIENT_DESCENT_MOVE = 1


def _c__uniform_quad_fn(distribution, integrand, dimensions,
                        boundaries, periodic=True):
    """Builds a JIT compiled function that executes a Quantum Monte Carlo
    integration over a many body quantum fluid.

    :param distribution: The wave function used as probability distribution
                      function (distribution).
    :param integrand: A function that evaluates the properties of the quantum
                      system after a random move.
    :param dimensions: The dimension of the configuration space where the
                       integration is going to be realized.
    :param boundaries:
    :param periodic: Indicates if the integration will be subject to periodic
                     boundary conditions. Defaults to ``False``.
    :return: The JIT compiled function that execute the Monte Carlo
             integration.
    """
    z_min, z_max = boundaries
    pdf = distribution

    @jit(nopython=True)
    def advance_system_configuration(z_drift_conf, move_amplitude):
        """Move the current configuration of the system. The moves are
        Gaussian displacements around the current position corrected by a
        drift term that depends on the time step of the evolution of the
        system.

        :param z_drift_conf: The current configuration.
        :param move_amplitude: The maximum amplitude of the displacement..
        """
        for kdx in range(dimensions):
            z_kdx, drift_kdx = z_drift_conf[kdx, MAIN_SLOT]

            uniform_spread = random.rand() - 0.5
            z_aux_kdx = z_kdx + uniform_spread * move_amplitude

            # If boundary conditions are periodic then the position
            # must be recast into the simulation box but from the
            # opposite side.
            if periodic:
                z_drift_conf[kdx, AUX_SLOT, 0] = z_min + z_aux_kdx % z_max
            else:
                z_drift_conf[kdx, AUX_SLOT, 0] = z_aux_kdx

    @jit(nopython=True)
    def uniform_quad(distribution_params, integrand_params, displace_params,
                     z_drift_walker, integrand_buffer, markov_chain, moves,
                     transient_moves, stabilization_moves, tol,
                     keep_chain=False, seed=False):
        """

        :param distribution_params:
        :param integrand_params:
        :param displace_params:
        :param z_drift_walker:
        :param integrand_buffer:
        :param markov_chain:
        :param moves:
        :param transient_moves:
        :param stabilization_moves:
        :param tol:
        :param keep_chain:
        :param seed:
        :return:
        """

        # Avoid `Untyped global name error` when executing the code in a
        # multiprocessing pool.
        rand = random.rand
        log = math.log

        # Get the movement amplitude.
        move_amp, = displace_params

        # Feed the numba random number generator with the given seed.
        if seed:
            random.seed(seed)

        # Initial iteration.
        main_conf = z_drift_walker[:, MAIN_SLOT]
        pdf_actual = pdf(distribution_params, z_drift_conf=main_conf)

        # Initialize accepted move variable.
        accepted_moves = 0

        # These random moves will be used only to sample the probability
        # function of the system, i.e., the square of the wave function.
        # They will not be used to calculate any physical property.
        for idx in range(stabilization_moves):

            advance_system_configuration(z_drift_walker, move_amp)

            aux_conf = z_drift_walker[:, AUX_SLOT]
            pdf_proposed = pdf(distribution_params, z_drift_conf=aux_conf)

            # Metropolis condition
            if pdf_proposed > 0.5 * log(rand()) + pdf_actual:
                # Accept the movement
                z_drift_walker[:, MAIN_SLOT] = z_drift_walker[:, AUX_SLOT]
                pdf_actual = pdf_proposed
                accepted_moves += 1

        # Evaluate the integrand for the first time.
        walker = z_drift_walker[:, MAIN_SLOT]
        integrand(integrand_params, walker, result=integrand_buffer)

        if keep_chain:
            # Save the local energy in the mapping
            markov_chain[0, :] = integrand_buffer[:]
        else:
            # Accumulate energy and energy squared
            markov_chain[0, :] += integrand_buffer[:]
            markov_chain[1, :] += integrand_buffer[:] ** 2

        # These random moves will be used to sample the probability function
        # of the system and to calculate some physical property after a
        # given number of transient moves.
        for idx in range(moves):
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for jdx in range(transient_moves):

                advance_system_configuration(z_drift_walker, move_amp)

                aux_conf = z_drift_walker[:, AUX_SLOT]
                pdf_proposed = pdf(distribution_params, z_drift_conf=aux_conf)

                # Accept the movement.
                if pdf_proposed > 0.5 * log(rand()) + pdf_actual:
                    z_drift_walker[:, MAIN_SLOT] = z_drift_walker[:, AUX_SLOT]
                    pdf_actual = pdf_proposed
                    accepted_moves += 1

            # Evaluate the integrand. We pass the non-flattened
            # ``integrand_buffer`` array, so the function must be able to
            # manage the array correctly.
            walker = z_drift_walker[:, MAIN_SLOT]
            integrand(integrand_params, walker, result=integrand_buffer)

            if keep_chain:
                # Save the local energy in the mapping
                markov_chain[idx + 1, :] = integrand_buffer[:]
            else:
                # Accumulate energy and energy squared
                markov_chain[0, :] += integrand_buffer[:]
                markov_chain[1, :] += integrand_buffer[:] ** 2

        if not keep_chain:
            # Average of energies.
            markov_chain[0, :] /= moves

            # Average of squared energies.
            markov_chain[1, :] /= moves

        # Stabilization and transient moves have to be accounted in
        # the acceptance ratio.
        acceptance_rate = (
            accepted_moves / (stabilization_moves + transient_moves * moves)
        )

        return markov_chain, acceptance_rate

    return uniform_quad


def _c__gradient_descent_quad_fn(distribution, integrand, dimensions,
                                 boundaries, periodic=True):
    """

    :param distribution:
    :param integrand:
    :param dimensions:
    :param boundaries:
    :param periodic:
    :return:
    """
    z_min, z_max = boundaries
    pdf = distribution

    @jit(nopython=True)
    def advance_system_configuration(z_drift_conf, time_step):
        """Move the current configuration of the system. The moves are
        Gaussian displacements around the current position corrected by a
        drift term that depends on the time step of the evolution of the
        system.

        :param z_drift_conf: The current configuration.
        :param time_step: Time-step :math:`\\tau` to adjust the drift displacement
                          due to the local drift velocity, i.e., the local
                          gradient. In addition, the standard deviation of the
                          gaussian movement is ``sqrt(time)``.
        """
        scale = math.sqrt(time_step)

        for kdx in range(dimensions):
            z_kdx, drift_kdx = z_drift_conf[kdx, MAIN_SLOT]

            gaussian_diffusion = random.normal(0, scale=scale)
            z_aux_kdx = z_kdx + drift_kdx * time_step + gaussian_diffusion

            # If boundary conditions are periodic then the position
            # must be recast into the simulation box but from the
            # opposite side.
            if periodic:
                z_drift_conf[kdx, AUX_SLOT, 0] = z_min + z_aux_kdx % z_max
            else:
                z_drift_conf[kdx, AUX_SLOT, 0] = z_aux_kdx

    @jit(nopython=True)
    def likelihood_end_start_log(z_drift_walker, time_step):
        """Calculates the quotient of the likelihood of a diffusive
        process with drift that occurs in one direction between the
        the same process in the inverse direction. See J. Toulouse,
        R. Assaraf and C. Umrigar, In Advances in Quantum Chemistry,
        Vol. 73, pp. 285–314. http://doi.org/10.1016/bs.aiq.2015.07.003

        :param z_drift_walker: The final configuration of the system.
        :param time_step: The time-step of the diffusive process.
        """
        arg_finish_start = 0.
        arg_start_finish = 0.

        for kdx in range(dimensions):
            #
            z_finish, drift_finish = z_drift_walker[kdx, AUX_SLOT]
            z_start, drift_start = z_drift_walker[kdx, MAIN_SLOT]

            z_end_start = z_finish - z_start
            arg_finish_start += (z_end_start - drift_start * time_step) ** 2
            arg_start_finish += (z_end_start + drift_finish * time_step) ** 2

        return -(arg_finish_start - arg_start_finish) / (2 * time_step)

    @jit(nopython=True)
    def gradient_descent_quad(distribution_params, integrand_params,
                              displace_params, z_drift_walker,
                              integrand_buffer, markov_chain, moves,
                              transient_moves, stabilization_moves, tol,
                              keep_chain=False, seed=False):
        """

        :param distribution_params:
        :param integrand_params:
        :param displace_params:
        :param z_drift_walker:
        :param integrand_buffer:
        :param markov_chain:
        :param moves:
        :param transient_moves:
        :param stabilization_moves:
        :param tol:
        :param keep_chain:
        :param seed:
        :return:
        """

        # Avoid `Untyped global name error` when executing the code in a
        # multiprocessing pool.
        rand = random.rand
        log = math.log

        # Get the time step.
        time_step, = displace_params

        # Feed the numba random number generator with the given seed.
        if seed:
            random.seed(seed)

        # Initial iteration.
        main_conf = z_drift_walker[:, MAIN_SLOT]
        pdf_actual = pdf(distribution_params, z_drift_conf=main_conf)

        # Initialize accepted moves variable.
        accepted_moves = 0

        # These random moves will be used only to sample the probability
        # function of the system, i.e., the square of the wave function.
        # They will not be used to calculate any physical property.
        for idx in range(stabilization_moves):

            advance_system_configuration(z_drift_walker, time_step)

            aux_conf = z_drift_walker[:, AUX_SLOT]
            pdf_proposed = pdf(distribution_params, z_drift_conf=aux_conf)

            like_quot_log = likelihood_end_start_log(z_drift_walker, time_step)

            # Metropolis condition.
            if pdf_proposed > 0.5 * like_quot_log + pdf_actual:
                # Accept the movement
                z_drift_walker[:, MAIN_SLOT] = z_drift_walker[:, AUX_SLOT]
                pdf_actual = pdf_proposed
                accepted_moves += 1

        # Evaluate the integrand for the first time.
        walker = z_drift_walker[:, MAIN_SLOT]
        integrand(integrand_params, walker, result=integrand_buffer)

        if keep_chain:
            # Save the local energy in the mapping
            markov_chain[0, :] = integrand_buffer[:]
        else:
            # Accumulate energy and energy squared
            markov_chain[0, :] += integrand_buffer[:]
            markov_chain[1, :] += integrand_buffer[:] ** 2

        # These random moves will be used to sample the probability function
        # of the system and to calculate some physical property after a
        # given number of transient moves.
        for idx in range(moves):
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for jdx in range(transient_moves):

                advance_system_configuration(z_drift_walker, time_step)

                aux_conf = z_drift_walker[:, AUX_SLOT]
                pdf_proposed = pdf(distribution_params, z_drift_conf=aux_conf)

                like_quot_log = likelihood_end_start_log(
                    z_drift_walker, time_step
                )

                # Accept the movement.
                if pdf_proposed > 0.5 * like_quot_log + pdf_actual:
                    z_drift_walker[:, MAIN_SLOT] = z_drift_walker[:, AUX_SLOT]
                    pdf_actual = pdf_proposed
                    accepted_moves += 1

            # Evaluate the integrand. We pass the non-flattened
            # ``integrand_buffer`` array, so the function must be able to
            # manage the array correctly.
            walker = z_drift_walker[:, MAIN_SLOT]
            integrand(integrand_params, walker, result=integrand_buffer)

            if keep_chain:
                # Save the local energy in the mapping
                markov_chain[idx + 1, :] = integrand_buffer[:]
            else:
                # Accumulate energy and energy squared
                markov_chain[0, :] += integrand_buffer[:]
                markov_chain[1, :] += integrand_buffer[:] ** 2

        if not keep_chain:
            # Average of energies.
            markov_chain[0, :] /= moves

            # Average of squared energies.
            markov_chain[1, :] /= moves

        # Stabilization and transient moves have to be accounted in
        # acceptance ratio.
        acceptance_rate = (
            accepted_moves / (stabilization_moves + transient_moves * moves)
        )

        return markov_chain, acceptance_rate

    return gradient_descent_quad


class MetropolisQuadrature(MonteCarloQuadrature):
    """Implements a Monte Carlo simulation over a many-body quantum system
    using Lieb-Liniger two-body trial functions.

    :param distribution: The wave function used as probability distribution
                         function (pdf).
    :param integrand: A function that evaluates the properties of the quantum
                      system after a random move.
    :param dimensions: The dimension of the configuration space where the
                       integration is going to be realized.
    :param periodic: Whether the boundary conditions of the problem are
                     periodic or not.
    :param boundaries: The boundaries of the simulation. By default they
                       are ``0`` and ``1``.
    """

    # The maximum number of moves used in the Monte Carlo Integration.
    STANDARD_QUAD_MOVES = 1000000

    # The number of transient moves used in the Monte Carlo integration.
    QUAD_TRANSIENT_MOVES = 20

    # The number of transient moves used in the Monte Carlo integration.
    QUAD_STABILIZATION_MOVES = 10000

    # The minimum relative tolerance to attain at the end of the Monte Carlo
    # integration.
    QUAD_TOL = 1e-2

    def __init__(self, distribution, integrand, dimensions, periodic=True,
                 boundaries=None):
        self._distribution = distribution
        self._integrand = integrand
        self._dimensions = dimensions
        self._periodic = periodic

        # By default restrict movements within the unit box.
        if boundaries is None:
            z_min, z_max = 0., 1.
        else:
            z_min = float(boundaries.get('z_min', 0.))
            z_max = float(boundaries.get('z_min', 1.))

        self._boundaries = (z_min, z_max)

        self._quad_fn = self.compile_quad_fn()

    @property
    def dimensions(self):
        return self._dimensions

    @property
    def distribution(self):
        return self._distribution

    @property
    def quad_fn(self):
        return self._quad_fn

    @abstractmethod
    def compile_quad_fn(self):
        """Builds a JIT compiled function that executes a Quantum Monte Carlo
        integration over a many body quantum fluid.

        :return: The JIT compiled function that execute the Monte Carlo
                 integration.
        """
        raise NotImplementedError

    def exec(self, distribution_params, integrand_params, displace_params,
             integrand_shape, moves=None, transient_moves=None,
             stabilization_moves=None, tolerance=None, keep_markov_chain=False,
             seed=None):
        """Executes the Monte Carlo quadrature for the current simulation.

        :param distribution_params:
        :param integrand_params: The parameters to be passed to the JIT compiled
                               functions. This argument is a tuple containing
                               other tuples of values.
        :param displace_params:
        :param integrand_shape:
        :param moves: The maximum number or random moves of the random walk.
        :param transient_moves: The number of transient moves before
                                proceeding to calculate the local energy and
                                other properties.
        :param stabilization_moves:
        :param tolerance: The absolute tolerance for the statistical error of the
                    integration.
        :param keep_markov_chain: Whether or not to keep the whole data chain
                           generated during the random walk in the quadrature.
        :param seed:
        :return: A tuple of three values: the mean energy, the mean of the
                 energy squared, and the number of accepted random moves.
        """
        moves = moves or self.STANDARD_QUAD_MOVES
        transient_moves = transient_moves or self.QUAD_TRANSIENT_MOVES
        stabilization_moves = (
            stabilization_moves or self.QUAD_STABILIZATION_MOVES
        )
        tolerance = tolerance or self.QUAD_TOL

        if transient_moves < 1:
            raise ValueError(
                'The number of transient moves must be greater or equal than '
                'one'
            )

        # If no seed is specified, seed the numpy RNG with a process-based
        # integer plus the current time in seconds, so the integer is
        # different for every process when working with the
        # ``multiprocessing`` module.
        if seed is None:
            np.random.seed(
                current_process().pid + int(time())
            )

            # Get a seed for the numba RNG. This will be passed to the
            # compiled quadrature function.
            seed = np.random.randint(0, high=2 ** 24)

        # We have to seed the numpy random number generator.
        np.random.seed(seed)

        random_sample = random.random_sample

        # Get simulation parameters.
        dimensions = self.dimensions
        periodic = self._periodic
        z_min, z_max = self._boundaries

        # Allocate space to save the configuration of the particles and
        # the drift velocity during the execution of a Metropolis-Hastings
        # random displacement. Also allocate space to save an auxiliary
        # configuration for the Metropolis-Hastings algorithm execution.
        z_drift_walker = np.zeros((dimensions, 2, 2), dtype=np.float64)

        # Initialize the random walk. We have to be sure that initially all
        # the particles are uniformly distributed over the entire space.
        z_drift_walker[:, MAIN_SLOT, 0] = (z_max - z_min) * random_sample(
            (dimensions,)) + z_min

        # For periodic boundary conditions move particles into the
        # unit cell.
        if periodic:
            z_drift_walker[:, MAIN_SLOT, 0] = z_min + (
                z_drift_walker[:, MAIN_SLOT, 0] % z_max
            )

        # For the sake of simplicity, we only manage, at most,
        # one-dimensional numpy arrays when saving the whole chain or/and
        # accumulating values during each iteration.
        # TODO: Analyze if this limitation can be eliminated.
        if keep_markov_chain and len(integrand_shape) > 2:
            raise ValueError(
                'Monte Carlo implementation is unable to manage integrands '
                'that yield numpy arrays with more than two-dimensions.'
            )

        # The shape of the final Markov chain.
        markov_chain_shape = (
            (moves + 1,) + integrand_shape if keep_markov_chain
            else (2,) + integrand_shape
        )

        # A buffer to save the physical properties of the system calculated
        # during the Markov chain.
        markov_chain = np.zeros(markov_chain_shape, dtype=np.float64)

        # A buffer to save temporarily the results calculated by the
        # during a Metropolis-Hastings displacement.
        integrand_buffer = np.zeros(integrand_shape, dtype=np.float64)

        # A tuple formed by the ``markov_chain`` buffer (not a copy) and \
        # the acceptance ratio of the random walk.
        markov_chain_and_ratio = self.quad_fn(
            distribution_params, integrand_params, displace_params,
            z_drift_walker, integrand_buffer, markov_chain, moves,
            transient_moves, stabilization_moves, tol=tolerance,
            keep_chain=keep_markov_chain, seed=seed
        )

        return MonteCarloResult(
            markov_chain_and_ratio, moves, z_drift_walker.copy(),
            is_markov_chain=keep_markov_chain
        )


class UniformQuadrature(MetropolisQuadrature):
    """Documentation"""

    def compile_quad_fn(self):
        """Builds a JIT compiled function that executes a Quantum Monte Carlo
        integration over a many body quantum fluid.

        :return: The JIT compiled function that execute the Monte Carlo
                 integration.
        """
        distribution = self.distribution
        integrand = self._integrand
        dimensions = self.dimensions
        boundaries = self._boundaries

        return _c__uniform_quad_fn(distribution, integrand, dimensions,
                                   boundaries)

    def exec(self, distribution_params, integrand_params, integrand_shape,
             move_amplitude=None, **options):
        """
        :param distribution_params:
        :param integrand_params:
        :param integrand_shape:
        :param move_amplitude: The maximum possible amplitude of a single random
                         move during the Monte Carlo quadrature.
        :param options:
        :return:
        """
        move_amplitude = move_amplitude or 1 / self.dimensions
        displace_params = (move_amplitude,)

        return super(UniformQuadrature, self).exec(
            distribution_params, integrand_params, displace_params,
            integrand_shape, **options
        )


class GradientDescentQuadrature(MetropolisQuadrature):
    """Documentation"""

    def compile_quad_fn(self):
        """Builds a JIT compiled function that executes a Quantum Monte Carlo
        integration over a many body quantum fluid.

        :return: The JIT compiled function that execute the Monte Carlo
                 integration.
        """
        distribution = self.distribution
        integrand = self._integrand
        dimensions = self.dimensions
        boundaries = self._boundaries

        return _c__gradient_descent_quad_fn(distribution, integrand,
                                            dimensions, boundaries)

    def exec(self, distribution_params, integrand_params, integrand_shape,
             time_step=1.0, **options):
        """

        :param distribution_params:
        :param integrand_params:
        :param integrand_shape:
        :param time_step:
        :param options:
        """
        time_step = time_step or 1.0
        displace_params = (time_step,)

        return super(GradientDescentQuadrature, self).exec(
            distribution_params, integrand_params, displace_params,
            integrand_shape, **options
        )
