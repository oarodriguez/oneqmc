"""
    oneqmc
    ~~~~~~

    OneQMC is a python library to perform Quantum Monte Carlo simulations
    on many-body quantum systems.

    OneQMC implements the Variational Monte Carlo (VMC) approach, and it uses
    the Metropolis-Hastings algorithm to evaluate an approximated value of
    several properties of a physical system, as the ground state energy and
    the structure factor, among other properties.

    :copyright: (c) 2016 by Abel Rodríguez.
    :license: BSD, see LICENSE for more details.
"""

# Multiprocessing managers
