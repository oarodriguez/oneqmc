from abc import ABC, abstractmethod
from multiprocessing.pool import Pool

from progressbar import Bar, ETA, Percentage, ProgressBar as _ProgressBar, Timer


class ProgressBar(_ProgressBar):
    """A class used to report the progress of a simulation.

    :param progress_marker: The character to display the progress of the
                            computations. Defaults to '■'.
    """

    #: The text to display in the progress bar.
    MESSAGE = 'Progress: '

    def __init__(self, progress_marker='■', *args, **kwargs):
        self._percent = Percentage()
        self._bar = Bar(marker=progress_marker)
        self._timer = Timer()
        self._eta = ETA()

        self._widgets = [
            self.MESSAGE, self._percent, ' ',
            self._bar, ' ',
            self._timer, ' | ', self._eta
        ]

        super(ProgressBar, self).__init__(widgets=self._widgets, *args,
                                          **kwargs)


class ParallelManager(ProgressBar):
    """

    """

    def __init__(self, processes=None, *args, **kwargs):
        super(ParallelManager, self).__init__(*args, **kwargs)
        self.processes = processes

    def _imap(self, kernel, data, progress_max_value=None):
        """

        :param kernel:
        :param data:
        :return:
        """

        processes = self.processes
        progress_bar = ProgressBar(
            widgets=self._widgets, max_value=progress_max_value
        )
        progress_bar.start()

        with Pool(processes=processes) as pool:
            result = []
            enum_imap = enumerate(pool.imap(kernel, data))
            for idx, process_result in enum_imap:
                result.append(process_result)
                progress_bar.update(idx + 1)

        progress_bar.finish()
        return result


class Kernel(ABC):
    """Represents the kernel, i.e., the callable object to be used in each
    one of the child processes managed by a :class:`ProgressBar` instance.
    """

    def __init__(self, log=True, log_level=1):
        self.log = log
        self.log_level = log_level

    @abstractmethod
    def __call__(self, *args, **kwargs):
        raise NotImplementedError
