"""
    oneqmc.defaults
    ~~~~~~~~~~~~~~~

    Default values for OneQMC configuration keys and specs.

    :copyright: (c) 2017 by O. A. Rodríguez.
    :license: BSD, see LICENSE for more details.
"""
from datetime import datetime

from oneqmc.config import ConfigSet

# Defaults for the keys of the configuration set.
META_KEY = 'meta'
SYSTEM_KEY = 'system'
VARIATIONAL_KEY = 'variational'
DENSITY_KEY = 'density'
CORRELATION_KEY = 'correlation'
STRUCTURE_FACTOR_KEY = 'structure_factor'
METROPOLIS_KEY = 'metropolis'
CONTROLLER_KEY = 'controller'

#
meta_defaults = {
    'name': 'Quantum_Simulation',
    'description': 'QMC simulation for a physical system',
    'tags': ['QMC'],
    'type': '',
    'start_time': None,
    'finish_time': None,
    'elapsed_time': None
}

controller_defaults = {
    'processes': None,
    'ranges': [],
    'estimators': []
}

# These variables are intended to be used for identification
meta_schema = ConfigSet(meta_defaults)
controller_schema = ConfigSet(controller_defaults)

metropolis_defaults = {
    'moves': 5000,
    'transient_moves': 100,
    'stabilization_moves': 25000,
    'tolerance': 1e-2,
    'keep_markov_chain': False,
    'seed': None
}

metropolis_uniform_defaults = metropolis_defaults.copy()
metropolis_uniform_defaults.update([('move_amplitude', 1e-2)])
metropolis_diffusive_defaults = metropolis_defaults.copy()
metropolis_diffusive_defaults.update([('time_step', 1e-2)])

evolution_defaults = {
    'energy_reference': 0,
    'initial_configuration': None,
    'blocks': 1,
    'total_time': 1.0,
    'time_steps': 100,
    'maximum_walkers': 7500,
    'tolerance': 1e-2,
    'keep_evolution': False
}

grid_config_defaults = {
    'major_grid': [],
    'minor_grid': None
}


def normalize_export_meta(config):
    """

    :param config:
    """
    cfg = config

    # Convert :class:`datetime` objects to strings.
    dts = [(k, cfg[k]) for k in ('finish_time', 'start_time')]
    for k, dt in dts:
        cfg[k] = str(dt) if isinstance(dt, datetime) else False

    cfg['elapsed_time'] = str(cfg['elapsed_time'])

    # Tags is a list, join its elements in a single string.
    cfg['tags'] = ', '.join([tag for tag in cfg['tags']])


def normalize_export_metropolis(config):
    """

    :param config:
    """
    cfg = config

    seed = cfg['seed']
    if seed is None:
        cfg['seed'] = False
