"""
    oneqmc.base
    ~~~~~~~~~~~

    Module that contains base classes and functions common to any
    quantum system and Quantum Monte Carlo simulation.

    :copyright: (c) 2016 by Abel Rodríguez.
"""
from abc import ABC, abstractmethod, abstractproperty

import numpy as np


class QuantumModel(ABC):
    """Represents a Quantum Model."""

    @abstractproperty
    def wave_function(self):
        raise NotImplementedError

    @abstractproperty
    def wave_function_log(self):
        raise NotImplementedError

    @abstractproperty
    def local_energy(self):
        raise NotImplementedError

    @abstractmethod
    def subsidiary_params(self, *params, **options):
        raise NotImplementedError

    @abstractmethod
    def compile_core(self, *args, **kwargs):
        raise NotImplementedError


class QuantumSystem(ABC):
    """Represents a quantum system."""

    @abstractmethod
    def energy(self, *args, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def structure_factor(self, *args, **kwargs):
        raise NotImplementedError


class MonteCarloQuadrature(ABC):
    """Represents a Monte Carlo multidimensional integration."""

    @abstractproperty
    def dimensions(self):
        raise NotImplementedError

    @abstractproperty
    def distribution(self):
        raise NotImplementedError

    @abstractproperty
    def quad_fn(self):
        raise NotImplementedError

    @abstractmethod
    def compile_quad_fn(self, *args, **kwargs):
        """Just-in-time compiles the optimized version of the Quantum
        Monte Carlo algorithm."""
        raise NotImplementedError

    @abstractmethod
    def exec(self, *args, **kwargs):
        """Starts the simulation."""
        raise NotImplementedError


class QMCResult(object):
    """Represents the result of a Quantum Monte Carlo simulation.
     The result object consists of the approximated mean value of the
     estimator of interest.

    :param data:
    """

    def __init__(self, data: np.ndarray):
        self._data = data
        result, error, acceptance_ratio = data.T

        self._result = result
        self._error = error
        self._acceptance_ratio = acceptance_ratio

    @property
    def result(self):
        return self._result

    @property
    def error(self):
        return self._error

    @property
    def acceptance_ratio(self):
        return self._acceptance_ratio

    def __str__(self):
        tpl = "Quantum Monte Carlo execution information:\n" \
              "     Mean energy:                    {:.5G}\n" \
              "     Error:                          {:.5G}\n" \
              "     Random walk acceptance ratio:   {:.5G}"
        return tpl.format(self._result, self._error, self._acceptance_ratio)

    def __repr__(self):
        return "QMCResult({:s})".format(repr(self._data))
