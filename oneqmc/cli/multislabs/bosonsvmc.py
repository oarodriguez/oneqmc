import datetime
import os
import re
import socket
from multiprocessing import current_process

import click
import numpy as np
from colorama import init, Fore, Style
from h5py import File
from numpy import zeros

from oneqmc.systems.multirods import bose_vmc_routines

# Default command lines options values
E0_VAR_DEFAULT = -1
ZM_VAR_DEFAULT = 0.25
RANDOM_MOVES_DEFAULT = 150000
PROCESSES_DEFAULT = 4
E0_VAR_STEPS_DEFAULT = 50
ZM_VAR_STEPS_DEFAULT = 50

RANDOM_MOVES_HELP_STR = '''Maximum number of proposed random moves. Default
value: {0:d}'''.format(RANDOM_MOVES_DEFAULT)

PROCESSES_HELP_STR = '''Number of parallel process to use during the
calculation. Default value: {0:d}'''.format(PROCESSES_DEFAULT)

# The default directory where the VMC data will be saved
VMC_DATA_DIR = 'VMC-DATA'

# Name of the groups where we will store data in HDF5 files
DATA_MAPS_GROUP = 'Multi-Slabs_ContactPotential/VariationalParameters_Maps'


def _vmc_parallel_unit_var_params(spec):
    """Execute VMC for the system with the specification given by *spec*."""
    u0, r, g0, n0, e0_var, z_var, moves = spec

    # This is the step. The inverse of the linear density.
    move_amp = 1.0 / n0

    # The value of the external potential magnitude.
    # We need it as the variational param e0var depends on it
    cur_process = current_process()
    term_msg_fmt = (
        "{fore_color} ++ Process {process}{reset} - Starting simulation "
        "for system with parameters [u0={:.5G}, r={:.5G}, g0={:.5G}, "
        "n0={:.5G}], variational parameters [e0_var={:.5G}, zm_var={:.5G}] "
        "and {:.6G} maximum random moves.\n"
    )
    term_msg = term_msg_fmt.format(
        u0, r, g0, n0, e0_var, z_var, int(moves),
        fore_color=Fore.LIGHTGREEN_EX, reset=Style.RESET_ALL,
        process=cur_process.name
    )
    print(term_msg)

    try:
        # The function that calculates the total mean energy per boson
        energy_functions = bose_vmc_routines(u0, r, g0, int(n0))
        mean_energy_fn = energy_functions[0]

        # The results of the simulation
        vmc_results = mean_energy_fn(z_var, int(moves), move_amp)
        mean_energy, mean_energy_error, acceptance_ratio = vmc_results

        term_msg_fmt = (
            "{fore_color} -- Process {process}{reset} finished.\n"
        )
        term_msg = term_msg_fmt.format(
            fore_color=Fore.LIGHTRED_EX,
            reset=Style.RESET_ALL,
            process=cur_process.name
        )
        print(term_msg)

        return np.array(
            [e0_var, z_var, mean_energy, mean_energy_error, acceptance_ratio]
        )
    except KeyboardInterrupt:
        print(Style.RESET_ALL)


# Collection of help strings
ZM_VAR_HELP_STR = '''The value of the variational matching point for the two
body functions. Default value: {0:.5G}'''.format(ZM_VAR_DEFAULT)

E0_VAR_STEPS_HELP_STR = '''The number of samples to take for the variational
energy for the one body functions. Default value: {0:d}'''.format(
    E0_VAR_STEPS_DEFAULT
)


@click.command()
@click.argument('params')
@click.option('-zm', '--zm-var', type=float, default=ZM_VAR_DEFAULT,
              help=ZM_VAR_HELP_STR)
@click.option('-evs', '--e0-var-steps', type=int,
              help=E0_VAR_STEPS_HELP_STR, default=E0_VAR_STEPS_DEFAULT)
@click.option('-nm', '--moves', type=int, default=RANDOM_MOVES_DEFAULT,
              help=RANDOM_MOVES_HELP_STR)
@click.option('-pp', '--processes', type=int, default=PROCESSES_DEFAULT,
              help=PROCESSES_HELP_STR)
def map_energy_as_e0_var(params, zm_var, e0_var_steps, moves, processes):
    """Program that calculates the ground state energy of a Bose gas within
    a multi-slabs system with the physical parameters specified in PARAMS.

    PARAMS: The parameters that define the system, i.e., the potential
    magnitude *u0*, the potential geometry factor *r*, and the interaction
    potential magnitude *g0*, and the average linear density *n0*.
    The parameters must be supplied as a string in the format *u0:r:g0:n0*,
    where the colon is used to separate the values of the parameters.

    By default the calculus of the ground state energy is done with one boson
    per
    """
    from multiprocessing import Pool

    init()

    print(
        Fore.YELLOW +
        """============================================================
    Starting the VMC calculation for the ground state
    as function of the number of bosons.
============================================================""" +
        Style.RESET_ALL
    )

    u0, r, g0, n0 = np.array(params.split(':'), dtype=np.float64)
    _zm_var = zm_var if zm_var is not None else 0.25

    cwd = os.getcwd()
    today = datetime.date.today()
    start_time = datetime.datetime.now()

    filename = 'bosons-data_{0}.h5'.format(today.strftime('%Y-%m-%d'))
    file_path = os.path.join(cwd, VMC_DATA_DIR, filename)

    try:
        datafile = File(file_path)
    except OSError:
        data_dir = os.path.join(cwd, VMC_DATA_DIR)
        os.mkdir(data_dir)
        datafile = File(file_path)
        print('INFO: The directory {0} has been created.'.format(data_dir))
        print('INFO: HDF5 data files will be saved in that directory.')

    try:
        class_group = datafile.create_group(DATA_MAPS_GROUP)
    except ValueError:
        class_group = datafile[DATA_MAPS_GROUP]

    e0_var_range = np.linspace(0.01, u0, e0_var_steps)
    parallel_spec = zeros([e0_var_range.shape[0], 7])
    parallel_spec[:] = [u0, r, g0, n0, 0, _zm_var, moves]
    parallel_spec[:, 4] = e0_var_range

    # Only create more processes if it is necessary
    if processes > 1:
        pool = Pool(processes=processes)
        simulation_data = pool.map(_vmc_parallel_unit_var_params,
                                   parallel_spec)
    else:
        simulation_data = list(
            map(_vmc_parallel_unit_var_params, parallel_spec))

    simulation_data = np.array(simulation_data)

    # Give the data set an unique name
    data_set_name = 'Map-{:s}'.format(start_time.strftime('%Y%m%d_%H:%M:%S'))
    try:
        del class_group[data_set_name]
        class_group[data_set_name] = simulation_data
    except KeyError:
        class_group[data_set_name] = simulation_data

    # Data set attributes
    system_params = np.array([u0, r, g0, n0], dtype=np.float64)
    system_params_spec = ', '.join([
        '[Potential Magnitude',
        'Width-Separation Ratio',
        'Contact Potential g0',
        'Boson Number]'
    ])
    data_element_spec = ', '.join([
        '[One-Body Variational Energy',
        'Two-Body Function Variational Match Point',
        'Mean Energy',
        'Mean Energy Error',
        'Accepted Random Moves]'
    ])

    data_set = class_group[data_set_name]
    now = datetime.datetime.now()
    elapsed_time = now - start_time

    data_set.attrs['SYSTEM_PARAMS'] = system_params
    data_set.attrs['SYSTEM_PARAMS_SPEC'] = system_params_spec
    data_set.attrs['TOTAL_RANDOM_MOVES'] = moves
    data_set.attrs['DATA_SHAPE'] = simulation_data.shape
    data_set.attrs['DATA_ELEMENT_SPEC'] = data_element_spec
    data_set.attrs['CREATION_DATE'] = start_time.strftime('%Y/%m/%d %H:%M:%S')
    data_set.attrs['COMPUTATION_ELAPSED_TIME'] = str(elapsed_time)

    # Write contents to disk
    datafile.flush()

    cwd = os.getcwd()
    print("""HDF5 File created with name {0}""".format(
        os.path.join(cwd, 'vmc_data', filename)))
    print("The data set {0} has been added to the file".format(data_set_name))


#############################################################################
############################################################################

# Collection of help strings
E0_VAR_HELP_STR = '''The value of the variational matching point for the one
body functions. Default value: {0:.5G}'''.format(E0_VAR_DEFAULT)

ZM_VAR_STEPS_HELP_STR = '''The number of samples to take for the variational
energy for the two body functions. Default value: {0:d}'''.format(
    ZM_VAR_STEPS_DEFAULT
)


@click.command()
@click.argument('params')
@click.option('-e0', '--e0-var', type=float, default=E0_VAR_DEFAULT,
              help=E0_VAR_HELP_STR)
@click.option('-zms', '--zm-var-steps', type=int,
              help=ZM_VAR_STEPS_HELP_STR, default=ZM_VAR_STEPS_DEFAULT)
@click.option('-nm', '--moves', type=int, default=RANDOM_MOVES_DEFAULT,
              help=RANDOM_MOVES_HELP_STR)
@click.option('-pp', '--processes', type=int, default=PROCESSES_DEFAULT,
              help=PROCESSES_HELP_STR)
def map_energy_as_zm_var(params, e0_var, zm_var_steps, moves, processes):
    """Program that calculates the ground state energy of a Bose gas within
    a multi-slabs system with the physical parameters specified in PARAMS.

    PARAMS: The parameters that define the system, i.e., the potential
    magnitude *u0*, the potential geometry factor *r*, and the interaction
    potential magnitude *g0*, and the average linear density *n0*.
    The parameters must be supplied as a string in the format *u0:r:g0:n0*,
    where the colon is used to separate the values of the parameters.

    By default the calculus of the ground state energy is done with one boson
    per
    """
    from multiprocessing import Pool

    init()

    print(
        Fore.YELLOW +
        """============================================================
    Starting the VMC calculation for the ground state
    as function of the number of bosons.
============================================================""" +
        Style.RESET_ALL
    )

    u0, r, g0, n0 = np.array(params.split(':'), dtype=np.float64)
    _e0_var = e0_var if e0_var != -1 else 0.2 * u0

    cwd = os.getcwd()
    today = datetime.date.today()
    start_time = datetime.datetime.now()

    filename = 'bosons-data_{0}.h5'.format(today.strftime('%Y-%m-%d'))
    file_path = os.path.join(cwd, VMC_DATA_DIR, filename)

    try:
        datafile = File(file_path)
    except OSError:
        data_dir = os.path.join(cwd, VMC_DATA_DIR)
        os.mkdir(data_dir)
        datafile = File(file_path)
        print('INFO: The directory {0} has been created.'.format(data_dir))
        print('INFO: HDF5 data files will be saved in that directory.')

    try:
        class_group = datafile.create_group(DATA_MAPS_GROUP)
    except ValueError:
        class_group = datafile[DATA_MAPS_GROUP]

    zm_var_range = np.linspace(0.01, 0.49, zm_var_steps)
    parallel_spec = zeros([zm_var_range.shape[0], 7])
    parallel_spec[:] = [u0, r, g0, n0, _e0_var, 0, moves]
    parallel_spec[:, 5] = zm_var_range

    # Only create more processes if it is necessary
    if processes > 1:
        pool = Pool(processes=processes)
        simulation_data = pool.map(_vmc_parallel_unit_var_params,
                                   parallel_spec)
    else:
        simulation_data = list(
            map(_vmc_parallel_unit_var_params, parallel_spec))

    simulation_data = np.array(simulation_data)

    # Give the data set an unique name
    data_set_name = 'Map-{:s}'.format(start_time.strftime('%Y%m%d_%H:%M:%S'))
    try:
        del class_group[data_set_name]
        class_group[data_set_name] = simulation_data
    except KeyError:
        class_group[data_set_name] = simulation_data

    # Data set attributes
    system_params = np.array([u0, r, g0, n0], dtype=np.float64)
    system_params_spec = ', '.join([
        '[Potential Magnitude',
        'Width-Separation Ratio',
        'Contact Potential g0',
        'Boson Number]'
    ])
    data_element_spec = ', '.join([
        '[One-Body Variational Energy',
        'Two-Body Function Variational Match Point',
        'Mean Energy',
        'Mean Energy Error',
        'Accepted Random Moves]'
    ])

    data_set = class_group[data_set_name]
    now = datetime.datetime.now()
    elapsed_time = now - start_time

    data_set.attrs['SYSTEM_PARAMS'] = system_params
    data_set.attrs['SYSTEM_PARAMS_SPEC'] = system_params_spec
    data_set.attrs['TOTAL_RANDOM_MOVES'] = moves
    data_set.attrs['DATA_SHAPE'] = simulation_data.shape
    data_set.attrs['DATA_ELEMENT_SPEC'] = data_element_spec
    data_set.attrs['CREATION_DATE'] = start_time.strftime('%Y/%m/%d %H:%M:%S')
    data_set.attrs['COMPUTATION_ELAPSED_TIME'] = str(elapsed_time)

    # Write contents to disk
    datafile.flush()

    cwd = os.getcwd()
    print("""HDF5 File created with name {0}""".format(
        os.path.join(cwd, 'vmc_data', filename)))
    print("The data set {0} has been added to the file".format(data_set_name))


############################################################################
############################################################################

# Name of the groups where we will store data in HDF5 files
DENSITY_MAPS_GROUP = 'Multi-Slabs_ContactPotential/SystemDensity_Maps'


@click.command()
@click.argument('params')
@click.option('-e0', '--e0-var', type=float, default=E0_VAR_DEFAULT,
              help=E0_VAR_HELP_STR)
@click.option('-zm', '--zm-var', type=float, default=ZM_VAR_DEFAULT,
              help=ZM_VAR_HELP_STR)
@click.option('-nm', '--moves', type=int, default=RANDOM_MOVES_DEFAULT,
              help=RANDOM_MOVES_HELP_STR)
@click.option('-pp', '--processes', type=int, default=PROCESSES_DEFAULT,
              help=PROCESSES_HELP_STR)
# @click.option('--dry-run', is_flag=True)
def map_energy_as_n0(params, e0_var, zm_var, moves, processes):
    """Maps an upper bound to the ground state energy of a Bose gas within a
    multi-slabs system with the physical parameters specified in PARAMS.

    PARAMS: The parameters that define the system, i.e., the potential
    magnitude *u0*, the potential geometry factor *r*, and the interaction
    potential magnitude *g0*, and the average linear density *n0*.
    The parameters must be supplied as a string in the format *u0:r:g0:n0*,
    where the colon is used to separate the values of the parameters.

    The mapping is done over an interval from *0* to *u0* for the variational
    energy of the one-body functions, and over an interval from *0* to *0.5*
    for the variational matching point of the two-body functions.
    """

    from multiprocessing import Pool

    init()

    print(
        Fore.YELLOW +
        """============================================================
    Starting the VMC calculation for the ground state
    as function of the number of bosons.
============================================================""" +
        Style.RESET_ALL
    )

    # Regular expression that matches the parameters specification of the
    # system, i.e. *u0:r:g0:[n0_start:n0_stop:n0_step]*.
    regex = re.compile('(.+?):(.+?):(.+?):\[(.+?)]')

    _u0, _r, _g0, _n0_spec = regex.findall(params)[0]
    u0, r, g0 = np.array([_u0, _r, _g0], dtype=np.float64)

    n0_start, n0_stop, n0_step = np.array(
        _n0_spec.split(':'), dtype=np.int32
    )
    _e0_var = e0_var if e0_var != -1 else 0.2 * u0

    cwd = os.getcwd()
    today = datetime.date.today()
    start_time = datetime.datetime.now()

    filename = 'bosons-data_{0}.h5'.format(today.strftime('%Y-%m-%d'))
    file_path = os.path.join(cwd, VMC_DATA_DIR, filename)

    try:
        datafile = File(file_path)
    except OSError:
        data_dir = os.path.join(cwd, VMC_DATA_DIR)
        os.mkdir(data_dir)
        datafile = File(file_path)
        print('INFO: The directory {0} has been created.'.format(data_dir))
        print('INFO: HDF5 data files will be saved in that directory.')

    try:
        class_group = datafile.create_group(DENSITY_MAPS_GROUP)
    except ValueError:
        class_group = datafile[DENSITY_MAPS_GROUP]

    n0_range = np.arange(n0_start, n0_stop + n0_step, n0_step)

    parallel_spec = np.zeros(n0_range.shape + (7,))
    parallel_spec[:] = [u0, r, g0, 0, _e0_var, zm_var, moves]
    parallel_spec[:, 3] = n0_range

    # ps_shape = parallel_spec.shape
    # new_shape = (ps_shape[0] * ps_shape[1], 7)

    # Only create more processes if it is necessary
    if processes > 1:
        pool = Pool(processes=processes)
        simulation_data = pool.map(
            _vmc_parallel_unit_var_params, parallel_spec
        )
    else:
        simulation_data = list(map(
            _vmc_parallel_unit_var_params, parallel_spec
        ))

    simulation_data = np.array(simulation_data)

    print(Style.RESET_ALL)

    # Give the data set an unique name
    data_set_name = 'Map-{:s}'.format(start_time.strftime('%Y%m%d_%H:%M:%S'))
    try:
        del class_group[data_set_name]
        class_group[data_set_name] = simulation_data
    except KeyError:
        class_group[data_set_name] = simulation_data

    # Data set attributes
    system_params = parallel_spec[:, :4]
    system_params_spec = ', '.join([
        '[Potential Magnitude',
        'Width-Separation Ratio',
        'Contact Potential g0',
        'Boson Number]'
    ])
    var_params_spec = ', '.join([
        '[One-Body Variational Energy',
        'Two-Body Function Variational Match Point]'
    ])
    data_element_spec = ', '.join([
        '[One-Body Variational Energy',
        'Two-Body Function Variational Match Point',
        'Mean Energy',
        'Mean Energy Error',
        'Accepted Random Moves]'
    ])

    data_set = class_group[data_set_name]
    now = datetime.datetime.now()
    elapsed_time = now - start_time

    data_set.attrs['SYSTEM_PARAMS'] = system_params
    data_set.attrs['SYSTEM_PARAMS_SPEC'] = system_params_spec
    data_set.attrs['VARIATIONAL_PARAMS_SPEC'] = var_params_spec
    data_set.attrs['TOTAL_RANDOM_MOVES'] = moves
    data_set.attrs['DATA_SHAPE'] = simulation_data.shape
    data_set.attrs['DATA_ELEMENT_SPEC'] = data_element_spec
    data_set.attrs['CREATION_DATE'] = start_time.strftime('%Y/%m/%d %H:%M:%S')
    data_set.attrs['COMPUTATION_ELAPSED_TIME'] = str(elapsed_time)

    # Write contents to disk
    datafile.flush()

    cwd = os.getcwd()
    print("""HDF5 File created with name {0}""".format(
        os.path.join(cwd, 'vmc_data', filename)))
    print("The data set {0} has been added to the file".format(data_set_name))


############################################################################
############################################################################


# Name of the groups where we will store data in HDF5 files
INTERACTION_MAPS_GROUP = 'Multi-Slabs/ContactPotential/InteractionStrength'


@click.command()
@click.argument('params')
@click.option('-e0', '--e0-var', type=float, default=E0_VAR_DEFAULT,
              help=E0_VAR_HELP_STR)
@click.option('-zm', '--zm-var', type=float, default=ZM_VAR_DEFAULT,
              help=ZM_VAR_HELP_STR)
@click.option('-nm', '--moves', type=int, default=RANDOM_MOVES_DEFAULT,
              help=RANDOM_MOVES_HELP_STR)
@click.option('-pp', '--processes', type=int, default=PROCESSES_DEFAULT,
              help=PROCESSES_HELP_STR)
# @click.option('--dry-run', is_flag=True)
def map_energy_as_g0(params, e0_var, zm_var, moves, processes):
    """Maps an upper bound to the ground state energy of a Bose gas within a
    multi-slabs system with the physical parameters specified in PARAMS.

    PARAMS: The parameters that define the system, i.e., the potential
    magnitude *u0*, the potential geometry factor *r*, and the interaction
    potential magnitude *g0*, and the average linear density *n0*.
    The parameters must be supplied as a string in the format *u0:r:[g0]:n0*,
    where the colon is used to separate the values of the parameters.

    The mapping is done over an interval from *0* to *u0* for the variational
    energy of the one-body functions, and over an interval from *0* to *0.5*
    for the variational matching point of the two-body functions.
    """

    from multiprocessing import Pool

    init()

    print(
        Fore.YELLOW +
        """============================================================
    Starting the VMC calculation for the ground state
    as function of the number of bosons.
============================================================""" +
        Style.RESET_ALL
    )

    # Regular expression that matches the parameters specification of the
    # system, i.e. *u0:r:g0:[n0_start:n0_stop:n0_step]*.
    regex = re.compile('(.+?):(.+?):\[(.+?)]:(.+)')

    _u0, _r, _g0_spec, _n0 = regex.findall(params)[0]
    u0, r, n0 = np.array([_u0, _r, _n0], dtype=np.float64)

    g0_start, g0_stop, g0_step = np.array(
        _g0_spec.split(':'), dtype=np.float64
    )
    _e0_var = e0_var if e0_var != -1 else 0.2 * u0

    cwd = os.getcwd()
    today = datetime.date.today()
    start_time = datetime.datetime.now()

    filename = 'bosons-data_{0}_{1}.h5'.format(
        today.strftime('%Y-%m-%d'),
        socket.gethostname()
    )
    file_path = os.path.join(cwd, VMC_DATA_DIR, filename)

    try:
        datafile = File(file_path)
    except OSError:
        data_dir = os.path.join(cwd, VMC_DATA_DIR)
        os.mkdir(data_dir)
        datafile = File(file_path)
        print('INFO: The directory {0} has been created.'.format(data_dir))
        print('INFO: HDF5 data files will be saved in that directory.')

    try:
        class_group = datafile.create_group(INTERACTION_MAPS_GROUP)
    except ValueError:
        class_group = datafile[INTERACTION_MAPS_GROUP]

    g0_range = np.arange(g0_start, g0_stop + g0_step, g0_step)

    parallel_spec = np.zeros(g0_range.shape + (7,))
    parallel_spec[:] = [u0, r, 0, n0, _e0_var, zm_var, moves]
    parallel_spec[:, 2] = g0_range

    # ps_shape = parallel_spec.shape
    # new_shape = (ps_shape[0] * ps_shape[1], 7)

    # Only create more processes if it is necessary
    if processes > 1:
        pool = Pool(processes=processes)
        simulation_data = pool.map(
            _vmc_parallel_unit_var_params, parallel_spec
        )
    else:
        simulation_data = list(map(
            _vmc_parallel_unit_var_params, parallel_spec
        ))

    simulation_data = np.array(simulation_data)

    print(Style.RESET_ALL)

    # Give the data set an unique name
    data_set_name = 'Map-{:s}'.format(start_time.strftime('%Y%m%d_%H:%M:%S'))
    try:
        del class_group[data_set_name]
        class_group[data_set_name] = simulation_data
    except KeyError:
        class_group[data_set_name] = simulation_data

    # Data set attributes
    system_params = parallel_spec[:, :4]
    system_params_spec = ', '.join([
        '[Potential Magnitude',
        'Width-Separation Ratio',
        'Contact Potential g0',
        'Boson Number]'
    ])
    var_params_spec = ', '.join([
        '[One-Body Variational Energy',
        'Two-Body Function Variational Match Point]'
    ])
    data_element_spec = ', '.join([
        '[One-Body Variational Energy',
        'Two-Body Function Variational Match Point',
        'Mean Energy',
        'Mean Energy Error',
        'Accepted Random Moves]'
    ])

    data_set = class_group[data_set_name]
    now = datetime.datetime.now()
    elapsed_time = now - start_time

    data_set.attrs['SYSTEM_PARAMS'] = system_params
    data_set.attrs['SYSTEM_PARAMS_SPEC'] = system_params_spec
    data_set.attrs['VARIATIONAL_PARAMS_SPEC'] = var_params_spec
    data_set.attrs['TOTAL_RANDOM_MOVES'] = moves
    data_set.attrs['DATA_SHAPE'] = simulation_data.shape
    data_set.attrs['DATA_ELEMENT_SPEC'] = data_element_spec
    data_set.attrs['CREATION_DATE'] = start_time.strftime('%Y/%m/%d %H:%M:%S')
    data_set.attrs['COMPUTATION_ELAPSED_TIME'] = str(elapsed_time)

    # Write contents to disk
    datafile.flush()

    cwd = os.getcwd()
    print("""HDF5 File created with name {0}""".format(
        os.path.join(cwd, 'vmc_data', filename)))
    print("The data set {0} has been added to the file".format(data_set_name))


############################################################################
############################################################################


E0_VAR_SAMPLES = 50
ZM_VAR_SAMPLES = 50

# Collection of help strings
E0_VAR_SAMPLES_HELP = 'The number of evenly spaced values between 0 and u0 ' \
                      'to take for the variational energy of the one-body ' \
                      'functions. Default value: {0:d}'.format(ZM_VAR_SAMPLES)

ZM_VAR_SAMPLES_HELP = 'The number of evenly spaced values between 0 and 0.5' \
                      'to take for the variational matching point of the ' \
                      'two-body functions. Default value: {0:d}'.format(
    ZM_VAR_SAMPLES)


@click.command()
@click.argument('params')
@click.option('-zvs', '--zm-var-samples', type=float,
              default=ZM_VAR_SAMPLES, help=ZM_VAR_SAMPLES_HELP)
@click.option('-evs', '--e0-var-samples', type=int,
              default=E0_VAR_SAMPLES, help=E0_VAR_SAMPLES_HELP)
@click.option('-nm', '--moves', type=int, default=RANDOM_MOVES_DEFAULT,
              help=RANDOM_MOVES_HELP_STR)
@click.option('-pp', '--processes', type=int, default=PROCESSES_DEFAULT,
              help=PROCESSES_HELP_STR)
@click.option('--dry-run', is_flag=True)
def map_mean_energy(params, zm_var_samples, e0_var_samples, moves, processes,
                    dry_run):
    """Maps an upper bound to the ground state energy of a Bose gas within a
    multi-slabs system with the physical parameters specified in PARAMS.

    PARAMS: The parameters that define the system, i.e., the potential
    magnitude *u0*, the potential geometry factor *r*, and the interaction
    potential magnitude *g0*, and the average linear density *n0*.
    The parameters must be supplied as a string in the format *u0:r:g0:n0*,
    where the colon is used to separate the values of the parameters.

    The mapping is done over an interval from *0* to *u0* for the variational
    energy of the one-body functions, and over an interval from *0* to *0.5*
    for the variational matching point of the two-body functions.
    """

    from multiprocessing import Pool

    init()

    print(
        Fore.YELLOW +
        """============================================================
    Starting the VMC calculation for the ground state
    as function of the number of bosons.
============================================================""" +
        Style.RESET_ALL
    )

    u0, r, g0, n0 = np.array(params.split(':'), dtype=np.float64)

    cwd = os.getcwd()
    today = datetime.date.today()
    start_time = datetime.datetime.now()

    filename = 'bosons-data_{0}.h5'.format(today.strftime('%Y-%m-%d'))
    file_path = os.path.join(cwd, VMC_DATA_DIR, filename)

    try:
        datafile = File(file_path)
    except OSError:
        data_dir = os.path.join(cwd, VMC_DATA_DIR)
        os.mkdir(data_dir)
        datafile = File(file_path)
        print('INFO: The directory {0} has been created.'.format(data_dir))
        print('INFO: HDF5 data files will be saved in that directory.')

    try:
        class_group = datafile.create_group(DATA_MAPS_GROUP)
    except ValueError:
        class_group = datafile[DATA_MAPS_GROUP]

    e0_var_range = np.linspace(0.05, u0 - 0.05, e0_var_samples)
    zm_var_range = np.linspace(0.05, 0.455, zm_var_samples)

    e0_var_mat, zm_var_mat = np.meshgrid(e0_var_range, zm_var_range)
    parallel_spec = np.zeros(e0_var_mat.shape + (7,))
    parallel_spec[:] = [u0, r, g0, n0, 0, 0, moves]
    parallel_spec[:, :, 4] = e0_var_mat
    parallel_spec[:, :, 5] = zm_var_mat

    ps_shape = parallel_spec.shape
    new_shape = (ps_shape[0] * ps_shape[1], 7)

    # Only create more processes if it is necessary
    if processes > 1:
        pool = Pool(processes=processes)
        simulation_data = pool.map(
            _vmc_parallel_unit_var_params,
            parallel_spec.reshape(new_shape)
        )
    else:
        simulation_data = list(
            map(_vmc_parallel_unit_var_params,
                parallel_spec,
                parallel_spec.reshape(new_shape)
                )
        )

    print(Style.RESET_ALL)

    simulation_data = np.array(simulation_data).reshape(
        e0_var_mat.shape + (5,)
    )

    # Give the data set an unique name
    data_set_name = 'Map-{:s}'.format(start_time.strftime('%Y%m%d_%H:%M:%S'))
    try:
        del class_group[data_set_name]
        class_group[data_set_name] = simulation_data
    except KeyError:
        class_group[data_set_name] = simulation_data

    # Data set attributes
    system_params = np.array([u0, r, g0, n0], dtype=np.float64)
    system_params_spec = ', '.join([
        '[Potential Magnitude',
        'Width-Separation Ratio',
        'Contact Potential g0',
        'Boson Number]'
    ])
    var_params_spec = ', '.join([
        '[One-Body Variational Energy',
        'Two-Body Function Variational Match Point]'
    ])
    data_element_spec = ', '.join([
        '[One-Body Variational Energy',
        'Two-Body Function Variational Match Point',
        'Mean Energy',
        'Mean Energy Error',
        'Accepted Random Moves]'
    ])

    data_set = class_group[data_set_name]
    now = datetime.datetime.now()
    elapsed_time = now - start_time

    data_set.attrs['SYSTEM_PARAMS'] = system_params
    data_set.attrs['SYSTEM_PARAMS_SPEC'] = system_params_spec
    data_set.attrs['VARIATIONAL_PARAMS_SPEC'] = var_params_spec
    data_set.attrs['TOTAL_RANDOM_MOVES'] = moves
    data_set.attrs['DATA_SHAPE'] = simulation_data.shape
    data_set.attrs['DATA_ELEMENT_SPEC'] = data_element_spec
    data_set.attrs['CREATION_DATE'] = start_time.strftime('%Y/%m/%d %H:%M:%S')
    data_set.attrs['COMPUTATION_ELAPSED_TIME'] = str(elapsed_time)

    # Write contents to disk
    if not dry_run:
        datafile.flush()

    cwd = os.getcwd()
    print("""HDF5 File created with name {0}""".format(
        os.path.join(cwd, 'vmc_data', filename)))
    print("The data set {0} has been added to the file".format(data_set_name))

# if __name__ == '__main__':
#     map_one_body_energy()
