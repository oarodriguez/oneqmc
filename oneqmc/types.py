from collections import Mapping, MutableMapping

import numpy as np


class MonteCarloResult(object):
    """

    :param data:
    :param rate:
    :param z_drift_conf:
    """

    @property
    def markov_chain(self):
        if self._is_markov_chain:
            return self._markov_chain
        raise AttributeError(
            "Instance has no 'markov_chain' attribute."
        )

    @property
    def mean_error_rate(self):
        """

        :return:
        """
        rate = self.rate
        if self._is_markov_chain and self._mean_error_rate is None:
            # If we have data that is a Markov chain, i.e., that
            # contains all the data of the random walk, then this
            # function calculates the averages and errors here..
            moves = self.moves
            dof = 1 if moves > 1 else 0
            markov_chain = self._markov_chain

            averages = markov_chain.mean(axis=0)
            errors = markov_chain.std(axis=0, ddof=dof) / np.sqrt(moves)
            rates = rate * np.ones_like(averages)

            self._mean_error_rate = np.stack((averages, errors, rates), axis=-1)

        return self._mean_error_rate

    def __init__(self, data, moves, z_drift_conf=None, is_markov_chain=False,
                 seed=None):
        # Keep a reference to the data.
        self._data = data  # type: np.ndarray

        self._markov_chain = data[0]
        self._is_markov_chain = is_markov_chain
        self._mean_error_rate = None

        rate = data[1]
        if not is_markov_chain:
            averages_data = self._markov_chain
            averages = averages_data[0]
            squares_averages = averages_data[1]

            # Pack the data, the error and the acceptance_rate
            self._mean_error_rate = self.pack_mean_error_rate(
                averages, squares_averages, rate, moves
            )

        #: The acceptance rate of the Monte Carlo integration.
        self.rate = rate

        #: The moves realized diring the Monte Carlo integration.
        self.moves = moves

        #: The last configuration of the positions of the particles
        #: after the simulation execution.
        self.z_drift_conf = z_drift_conf

        #: The random seed that gave rise to the current
        #: configuration and results.
        self.seed = seed

    @staticmethod
    def pack_mean_error_rate(averages, squares_averages, rate, moves):
        """

        :param averages:
        :param squares_averages:
        :return:
        """

        # It's necessary to use the absolute value of :math:`\langle E^2
        # \rangle - \langle E \rangle^2` as for an eigen-state of the
        # Hamiltonian this value may be very small though negative.
        errors = np.sqrt(np.fabs(squares_averages - averages ** 2))
        if moves > 1:
            errors /= np.sqrt(moves - 1)

        # We add the acceptance ratio as an additional element of the
        # resulting array.
        rates = rate * np.ones_like(averages)

        # Stack the properties in triplets.
        return np.stack((averages, errors, rates), axis=-1)


def items_dotted(mapping: Mapping):
    items = []
    for k, v in mapping.items():
        if isinstance(v, Mapping):
            sub_items = items_dotted(v)  # type: list
        else:
            items.append((k, v))
            continue
        for sk, sv in sub_items:
            fk = '.'.join([k, sk])
            items.append((fk, sv))
    return items


class FixedDict(MutableMapping):
    """Type that behaves as a regular ``dict`` with the constraint that
     once instantiated no additional keys can be added to it nor any
     existing key can be deleted. Only the initial keys can be modified.

    :param seq: A sequence or mapping that initializes the instance.
    :param kwargs: Keywords to initialize the instance.

    As this type behaves exactly as a dictionary with the constraints
    explained before, see
    ``https://docs.python.org/3/library/stdtypes.html#dict`` to
    understand the ways to create an instance.
    """

    __slots__ = ('data',)

    def __init__(self, *seq, **kwargs):
        if not seq and not kwargs:
            raise ValueError('Initial map or sequence is required')

        # FIXME: This may cause a pickling error.
        self.data = dict(*seq, **kwargs)

    def items(self):
        return self.data.items()

    def keys(self):
        return self.data.keys()

    def values(self):
        return self.data.values()

    def __getitem__(self, key):
        if key not in self.data:
            raise KeyError('{}'.format(key))
        return self.data[key]

    def __setitem__(self, key, value):
        if key not in self.data:
            raise KeyError('{}'.format(key))
        self.data[key] = value

    def __delitem__(self, key):
        raise KeyError("can not delete '{}' key".format(key))

    def __iter__(self):
        return iter(self.data)

    def __len__(self):
        return len(self.data)

    def __repr__(self):
        # TODO: Is this OK? :\
        return '{0}({1})'.format(self.__class__.__name__, self.data)

    def __reduce__(self):
        return (
            self.__class__,
            (tuple((k, v) for k, v in self.items()),)
        )


class DeepFixedDict(FixedDict):
    """This is a type with a similar behavior as :class:`FixedDict`,
    the main difference is that this type converts any dictionary
    contained in any of its keys to a :class:`DeepFixedDict`
    instance recursively. It also overloads the :meth:`update`
    method so keys are updated taking into account its deep into
    the dictionary tree.

    :param seq: A sequence or mapping that initializes the instance.
    :param kwargs: Keywords to initialize the instance.
    """
    __slots__ = ()

    def __init__(self, *seq, **kwargs):
        if not seq and not kwargs:
            raise ValueError('Initial map or sequence is required')

        super(DeepFixedDict, self).__init__(*seq, **kwargs)

        # Mapping items will be converted to :class:`DeepFixedDict`
        # instances. Direct access to :attr:`data` is required.
        for key, value in self.data.items():
            if isinstance(value, Mapping):
                self.data[key] = DeepFixedDict(value)

    def __setitem__(self, key, value):
        if isinstance(self[key], DeepFixedDict):
            raise ValueError("'{}' key can only be updated, "
                             "not replaced".format(key))
        self.data[key] = value

    def update(self, *args, **kwargs):
        other = dict(*args, **kwargs)
        for key, value in other.items():
            if isinstance(self[key], DeepFixedDict):
                self[key].update(value)
            else:
                self[key] = value
