from oneqmc.algorithms.vmc_onedim import GradientDescentQuadrature, \
    UniformQuadrature
from oneqmc.config import contains_
from oneqmc.defaults import DENSITY_KEY, METROPOLIS_KEY, SYSTEM_KEY, \
    VARIATIONAL_KEY, metropolis_diffusive_defaults, metropolis_uniform_defaults
from oneqmc.simulation import Estimator
from oneqmc.systems.multirods.config import density_defaults, system_defaults, \
    variational_defaults
from .model import DriftModel, LiebModel


class EnergyEstimator(Estimator):
    """Estimates the energy of the multi-rods system with a Lieb trial
    function. The estimation is done using a Metropolis-Hastings quadrature
    with uniform moves in the configuration space.

    :param config: A mapping object with the configuration of the
                          physical system.
    """
    __exec_config__ = {
        SYSTEM_KEY: system_defaults,
        VARIATIONAL_KEY: variational_defaults,
        METROPOLIS_KEY: metropolis_uniform_defaults,
        'grid_elements': None
    }

    __instance_config__ = {
        SYSTEM_KEY: system_defaults
    }

    #: Configuration parameters (and its default values) of the system
    #: needed to estimate the energy.
    model_type = LiebModel

    #: Physical model used by the estimator.
    quad_type = UniformQuadrature

    #: The type of the quadrature employed to calculate the expected
    #: value of the energy.
    def __init__(self, config):
        super().__init__(config)

        system_config = config[SYSTEM_KEY]
        self.model = self.model_type(**system_config)

        self.quad_fn = self.make_quad(self.model)

    def make_quad(self, model):
        """Builds the Metropolis-Hastings quadrature object that executes
        that estimates the energy.

        :param model: The physical model object for which the energy will be
                      estimated.
        :return: The quadrature object.
        """
        distribution = model.wave_function_log
        integrand = model.local_energy_to_buffer
        dimensions = model._dimensions

        return self.quad_type(distribution, integrand, dimensions)

    def eval(self, other=None):
        """Evaluates the estimator for the configuration given by
        the ``eval_config``.

        :param other: A mapping with the configuration of the
                      system.
        :return: The result object of the Metropolis integration.
        """
        config = other or self.config
        var_spec = config[VARIATIONAL_KEY]
        rl = var_spec['one_body_fn']['box_period_ratio']
        rm = var_spec['two_body_fn']['match_point']

        wf_params = (rm, rl) + self.model.subsidiary_params(rm)
        estimator_params = wf_params
        estimator_shape = (1,)
        options = config[METROPOLIS_KEY]

        return self.quad_fn.exec(wf_params, estimator_params, estimator_shape,
                                 **options)

    @classmethod
    def is_instance_param(cls, config_param):
        return contains_(cls.__instance_config__, config_param)


class GradientEnergyEstimator(EnergyEstimator):
    """Estimates the energy of the multi-rods system with a Lieb trial
    function. The estimation is done using a Metropolis-Hastings quadrature
    with diffusive, gaussian moves in the configuration space. This
    diffusive move is corrected by a drift term that depends on gradient
    of the the wave function.
    """

    __exec_config__ = {
        SYSTEM_KEY: system_defaults,
        VARIATIONAL_KEY: variational_defaults,
        METROPOLIS_KEY: metropolis_diffusive_defaults
    }

    model_type = DriftModel

    quad_type = GradientDescentQuadrature


class OBDMEstimator(EnergyEstimator):
    """Estimates the one-body density matrix of the multi-rods system with a
    Lieb trial function. The estimation is done using a Metropolis-Hastings
    quadrature with uniform moves in the configuration space. See
    :class:`EnergyEstimator` documentation for usage details.
    """

    #: Configuration parameters (and its default values) of the system
    #: needed to estimate the energy.
    __exec_config__ = {
        SYSTEM_KEY: system_defaults,
        VARIATIONAL_KEY: variational_defaults,
        DENSITY_KEY: density_defaults,
        METROPOLIS_KEY: metropolis_uniform_defaults,
        'grid_elements': None
    }

    def make_quad(self, model):
        """Builds the Metropolis-Hastings quadrature object that executes
        that estimates the energy.

        :param model: The physical model object for which the energy will be
                      estimated.
        :return: The quadrature object.
        """
        distribution = model.wave_function_log
        integrand = model.local_one_body_density
        dimensions = model._dimensions

        return self.quad_type(distribution, integrand, dimensions)

    def eval(self, other=None):
        """Evaluates the estimator for the configuration given by
        the ``estimator_spec``.

        :param estimator_spec: A mapping with the configuration of the
                               system.
        :return: The result object of the Metropolis integration.
        """
        config = other or self.config
        var_spec = config[VARIATIONAL_KEY]
        rl = var_spec['one_body_fn']['box_period_ratio']
        rm = var_spec['two_body_fn']['match_point']

        density_config = config[DENSITY_KEY]
        z_rel = density_config['relative_distance']

        wf_params = (rm, rl) + self.model.subsidiary_params(rm)
        estimator_params = wf_params, (z_rel,)
        estimator_shape = (1,)
        options = config[METROPOLIS_KEY]

        return self.quad_fn.exec(wf_params, estimator_params, estimator_shape,
                                 **options)


class GradientOBDMEstimator(OBDMEstimator):
    """Estimates the energy of the multi-rods system with a Lieb trial
    function. The estimation is done using a Metropolis-Hastings quadrature
    with diffusive, gaussian moves in the configuration space. This
    diffusive move is corrected by a drift term that depends on gradient
    of the the wave function.
    """

    __exec_config__ = {
        SYSTEM_KEY: system_defaults,
        VARIATIONAL_KEY: variational_defaults,
        DENSITY_KEY: density_defaults,
        METROPOLIS_KEY: metropolis_diffusive_defaults
    }

    model_type = DriftModel

    quad_type = GradientDescentQuadrature
