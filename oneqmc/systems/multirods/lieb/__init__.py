"""
    oneqmc.multirods.lieb
    ~~~~~~~~~~~~~~~~~~~~~

    This package contains the main routines to perform Quantum Monte Carlo
    techniques on a Bose or Fermi gas constrained within a multi-rods
    structure in one dimension with a Lieb-type trial function.

    :copyright: (c) 2017 by Abel Rodríguez.
    :license: BSD, see LICENSE for more details.
"""

#
from .model import DriftModel, LiebModel
from .variational import EnergyEstimator, GradientEnergyEstimator, \
    OBDMEstimator
