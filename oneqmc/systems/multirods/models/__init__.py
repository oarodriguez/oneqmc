"""
    oneqmc.multirods.models
    ~~~~~~~~~~~~~~~~~~~~~~~

    This package contains modules to perform Quantum Monte Carlo calculations
    on a Bose gas constrained within a multi-rods structure in one dimension
    using several different variational trial functions.

    :copyright: (c) 2016 by Abel Rodríguez.
"""
