"""
    oneqmc.multirods
    ~~~~~~~~~~~~~~~~

    This package contains the main routines to perform Quantum Monte Carlo
    techniques on a Bose or Fermi gas constrained within a multi-rods
    structure in one dimension.

    :copyright: (c) 2016 by Abel Rodríguez.
    :license: BSD, see LICENSE for more details.
"""

#
from .config import system_defaults, variational_defaults

# Main classes.
from .lieb import DriftModel, LiebModel
