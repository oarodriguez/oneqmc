from collections import OrderedDict
from math import sqrt, fabs, cos, sin, pi, exp, cosh, tanh, sinh, tan, log, atan

from numba import vectorize, float64, jit
from scipy.optimize import brentq

from oneqmc.systems.multirods.ideal import one_body_eigen_energy
from ..lieb import LiebModel


def _compiled_model(potential_magnitude,
                    geometric_ratio,
                    interaction_strength,
                    particles_number,
                    evaluate_drift=False):
    """Returns JIT compiled functions used to execute a Quantum Monte Carlo
    simulation over a many body quantum fluid.

    The quantum system corresponds to an 1D interacting Bose gas constrained
    by a multi-slabs structure. The multi-slabs structure is modeled through
    a Kronig-Penney potential of magnitude :math:`V_0`, whose potential
    barriers have a width :math:`b` and a separation :math:`a` between any
    two adjacent barriers. The interaction of any two bosons is assumed to
    be a contact potential, :math:`g_0 \delta(z_1 - z_2)`.

    The returned routines calculate an approximation for the energy of
    the ground state of a multi-slabs system using the variational method.

    :param potential_magnitude: The potential magnitude in dimension-less
                                units.
    :param geometric_ratio: The ratio width/separation of the barriers,
                            i.e. :math:`b / a`.
    :param interaction_strength: The interaction potential strength :math:`\gamma`
    :param particles_number: The density of bosons within an unit cell of the
                         system.
    :return: A dictionary with JIT compiled functions that calculates the
    approximation of
    the ground state energy.
    """

    # IMPORTANT: In order to avoid compiler errors (with llvmlite and/or
    # numba as ``value token required``) we have to convert these arguments to
    # floats and integers.
    u0 = float(potential_magnitude)
    r = float(geometric_ratio)
    g = float(interaction_strength)
    nb = int(particles_number)

    z_a, z_b = 1 / (1 + r), r / (1 + r)
    e0 = float(one_body_eigen_energy(u0, r))

    k1 = sqrt(e0)
    kp1 = sqrt(u0 - e0)

    # Indicates when a Bose gas is free (no external potential).
    gas_is_free = (u0 < 1e-10) or (r < 1e-10)

    # Indicates when a Bose gas is ideal (non-interacting particles).
    gas_is_ideal = (g == 0)

    @vectorize([float64(float64, float64, float64, float64, float64, float64)])
    def two_body_ufunc(rm, k2, beta, r_off, am, r):
        """Computes the two-body correlation Jastrow function.

        :param params:
        :return:
        """
        if g == 0:  return 1.0
        if r < fabs(rm):
            return am * cos(k2 * (r - r_off))
        else:
            return sin(pi * r) ** beta

    @jit(nopython=True)
    def wave_function(parameters_set, z_drift_conf):
        """Computes the variational wave function of a system of
        bosons in a specific configuration.

        :param parameters_set:
        :param z_drift_conf:
        :return:
        """
        wf_log = wave_function_log(parameters_set, z_drift_conf)
        return exp(wf_log)

    @jit(nopython=True)
    def wave_function_log(parameters_set, z_drift_conf):
        """Computes the variational wave function of a system of bosons in
        a specific configuration.

        :param parameters_set:
        :param z_drift_conf:
        :return:
        """
        wf_log = 0.

        # Unpack the parameters.
        wf_params = parameters_set

        rm, rl, k2, beta, r_off, am = wf_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        if gas_is_free and gas_is_ideal:
            return wf_log

        for idx in range(nb):

            # Drift velocity for ``idx`` particle.
            drift_velocity_idx = 0

            # Do not add contributions from one-body terms if the external
            # potential is zero.
            if not gas_is_free:

                z_idx = z_drift_conf[idx, 0]

                # Position in units of potential period.
                z_period_idx = (z_idx * rl) % 1

                if 1 / (1 + r) < z_period_idx:

                    # One-body term.
                    one_body_fn = cosh(
                        kp1 * (z_period_idx - 1 + 0.5 * r / (1 + r))
                    )

                    # One-body logarithmic derivative. As ``kp1`` is given in
                    # units of potential period length, we have ot multiply
                    # by ``rl`` in order to convert it to units of simulation
                    # box length.
                    if evaluate_drift:
                        one_body_fn_idx_lnd = kp1 * rl * (
                            tanh(kp1 * (z_period_idx - 1 + 0.5 * r / (1 + r)))
                        )
                        drift_velocity_idx += one_body_fn_idx_lnd

                # Region where the potential is zero.
                else:
                    one_body_fn = fabs(
                        sqrt(1 + u0 / e0 * sinh(
                            0.5 * sqrt(u0 - e0) * r / (1. + r)
                        ) ** 2.0) * cos(
                            k1 * (z_period_idx - 0.5 / (1 + r))
                        )
                    )

                    # One-body logarithmic derivative in units of simulation
                    # box length.
                    if evaluate_drift:
                        one_body_fn_idx_lnd = (
                            -k1 * rl * tan(k1 * (z_period_idx - 0.5 / (1 + r)))
                        )
                        drift_velocity_idx += one_body_fn_idx_lnd

                wf_log += log(one_body_fn)

            # Do not add contributions from two-body terms if the interaction
            # magnitude is zero.
            if not gas_is_ideal:
                # Total contribution to wave function from two-body terms.
                two_body_wf_log = 0.

                # Position in units of simulation box size.
                z_idx = z_drift_conf[idx, 0]

                # For the particles with ``jdx < idx`` there is no contribution
                # to the wave function, only to the drift velocity.
                if evaluate_drift:
                    for jdx in range(idx):

                        # Position in units of simulation box length.
                        z_jdx = z_drift_conf[jdx, 0]

                        z_ij = z_idx - z_jdx
                        sgn = -1 if z_ij < 0 else 1
                        r_ij = fabs(z_ij)
                        sgn = -sgn if r_ij > 0.5 else sgn

                        # If the ``jdx`` particle is outside the simulation box
                        # around the particle ``idx`` then the magnitude
                        # ``r_ij`` # is greater that ``0.5``. In this case we
                        # take the "image" # of particle ``jdx``, so the
                        # distance between particles is effectively
                        # ``1 - r_ij``.
                        if r_ij > 0.5:
                            r_ij = 1 - r_ij

                        if r_ij < fabs(rm):
                            # The drift velocity contribution is the
                            # logarithmic derivative of the corresponding
                            # two-body term.
                            drift_velocity_idx_jdx = (
                                -k2 * tan(k2 * (r_ij - r_off)) * sgn
                            )

                        else:
                            # The drift velocity contribution is the
                            # logarithmic derivative of the corresponding
                            # two-body term.
                            drift_velocity_idx_jdx = (
                                pi * beta / tan(pi * r_ij) * sgn
                            )

                        # Add contribution to drift velocity.
                        drift_velocity_idx += drift_velocity_idx_jdx

                # For the particles with ``jdx > idx`` there is contribution
                # to both the wave function and the drift velocity.
                for jdx in range(idx + 1, nb):

                    # Position in units of simulation box length.
                    z_jdx = z_drift_conf[jdx, 0]

                    z_ij = z_idx - z_jdx
                    sgn = -1 if z_ij < 0 else 1
                    r_ij = fabs(z_ij)
                    sgn = -sgn if r_ij > 0.5 else sgn

                    # If the ``jdx`` particle is outside the simulation box
                    # around the particle ``idx`` then the magnitude
                    # ``r_ij`` # is greater that ``0.5``. In this case we
                    # take the "image" # of particle ``jdx``, so the
                    # distance between particles is effectively
                    # ``1 - r_ij``.
                    if r_ij > 0.5:
                        r_ij = 1 - r_ij

                    if r_ij < fabs(rm):
                        # Two-body term.
                        two_body_term = fabs(am * cos(k2 * (r_ij - r_off)))

                        # Drift velocity contribution.
                        if evaluate_drift:
                            drift_velocity_idx_jdx = (
                                -k2 * tan(k2 * (r_ij - r_off)) * sgn
                            )
                            drift_velocity_idx += drift_velocity_idx_jdx

                    else:
                        # Two-body term.
                        two_body_term = fabs(sin(pi * r_ij) ** beta)

                        # Drift velocity contribution.
                        if evaluate_drift:
                            drift_velocity_idx_jdx = (
                                pi * beta / tan(pi * r_ij) * sgn
                            )
                            drift_velocity_idx += drift_velocity_idx_jdx

                    two_body_wf_log += log(two_body_term)

                wf_log += two_body_wf_log

            if evaluate_drift:
                z_drift_conf[idx, 1] = drift_velocity_idx

        return wf_log

    @jit(nopython=True)
    def multi_rods_potential(rl, z_conf):
        """Calculates the potential energy of the Bose gas due to the
         external potential.

         :param z_conf: The current configuration of the positions of the
                        particles.
         :return:
        """
        z_idx = (z_conf * rl) % 1

        #: We know the potential magnitude in units of potential period,
        #: so we have to multiply by `rs ** 2` in order to convert it to
        #: simulation box length units.
        return u0 * rl ** 2 if z_a < z_idx else 0

    @jit(nopython=True)
    def particle_local_energy_parts(index, parameters_set, z_drift_conf):
        """Computes the local energy for a given configuration of the
        position of the bodies. The kinetic energy of the hamiltonian is
        computed through central finite differences.

        :param parameters_set:
        :param wf_params:
        :param z_drift_conf: The current configuration of the positions of the
                       particles.
        :return: The local energy.
        """
        one_body_kinetic_energy = 0
        two_body_kinetic_energy = 0
        drift_velocity_magnitude = 0
        potential_energy = 0

        # Unpack the parameters.
        wf_params = parameters_set

        # The variational parameters ``rm, k2, beta, r_off, am``.
        rm, rl, k2, beta, r_off, am = wf_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        if gas_is_free and gas_is_ideal:
            return 0., 0.

        # Short alias.
        idx = index

        # Drift velocity for the ``idx`` particle.
        drift_velocity_idx = 0

        if not gas_is_free:

            # Position in units of simulation box.
            z_idx = z_drift_conf[idx, 0]

            # Position in units of potential period.
            z_period_idx = (z_idx * rl) % 1

            if 1 / (1 + r) < z_period_idx:
                # External potential is non-zero in this region.

                # One-body eigen-energy in units of simulation box length.
                one_body_eigen_energy = (e0 - u0) * rl ** 2

                # One-body logarithmic derivative. As `kp1` is given in
                # units of potential period length, we have ot multiply
                # by ``rs`` in order to convert it to units of simulation
                # box length.
                one_body_fn_idx_lnd = kp1 * rl * (
                    tanh(kp1 * (z_period_idx - 1 + 0.5 * r / (1 + r)))
                )
            else:
                # One-body eigen-energy in units.
                one_body_eigen_energy = e0 * rl ** 2

                # One-body logarithmic derivative in units of simulation
                # box length.
                one_body_fn_idx_lnd = (
                    -k1 * rl * tan(k1 * (z_period_idx - 0.5 / (1 + r)))
                )

            one_body_kinetic_energy += (
                one_body_eigen_energy + one_body_fn_idx_lnd ** 2
            )
            drift_velocity_idx += one_body_fn_idx_lnd

            # Accumulate the potential energy.
            potential_energy += multi_rods_potential(rl, z_idx)

        # Do not add contributions from two-body terms if the interaction
        # magnitude is zero.
        if not gas_is_ideal:

            # Position in units of simulation box.
            z_idx = z_drift_conf[idx, 0]

            for jdx in range(nb):
                # Do not account diagonal terms.
                if jdx == idx:
                    continue

                # Position in units of simulation box length.
                z_jdx = z_drift_conf[jdx, 0]

                # Relative distance between particles.
                z_ij = z_idx - z_jdx
                sgn = -1 if z_ij < 0 else 1
                r_ij = fabs(z_ij)
                sgn = -sgn if r_ij > 0.5 else sgn

                # If the ``jdx`` particle is outside the simulation box
                # around the particle ``idx`` then the magnitude ``r_ij``
                # is greater that ``0.5``. In this case we take the "image"
                # of particle ``jdx``, so the distance between particles is
                # effectively ``1 - r_ij``.
                if r_ij > 0.5:
                    r_ij = 1 - r_ij

                if r_ij < fabs(rm):
                    # Two-body eigen-energy in units of simulation box
                    # length.
                    two_body_eigen_energy = k2 ** 2

                    # Logarithmic derivative of the two-body term.
                    two_body_fn_idx_jdx_lnd = (
                        -k2 * tan(k2 * (r_ij - r_off)) * sgn
                    )

                else:
                    # Two-body eigen-energy in units of simulation box
                    # length.
                    two_body_eigen_energy = -pi ** 2 * beta * (
                        (beta - 1) / (tan(pi * r_ij) ** 2) - 1
                    )

                    # Logarithmic derivative of the two-body term.
                    two_body_fn_idx_jdx_lnd = (
                        pi * beta / tan(pi * r_ij) * sgn
                    )

                two_body_kinetic_energy += (
                    two_body_eigen_energy + two_body_fn_idx_jdx_lnd ** 2
                )

                # Add the contribution from the two-body terms to the
                # drift # velocity.
                drift_velocity_idx += two_body_fn_idx_jdx_lnd

        # Accumulate to the drift velocity squared magnitude.
        drift_velocity_magnitude += drift_velocity_idx ** 2

        kinetic_energy = (
            one_body_kinetic_energy + two_body_kinetic_energy -
            drift_velocity_magnitude
        )

        return kinetic_energy, potential_energy

    @jit(nopython=True)
    def local_energy_parts(parameters_set, z_drift_conf):
        """Computes the different contributions to the local energy for
        a given configuration of the position of the bodies. These
        contributions are the kinetic energy and the potential energy
        of each particle.

        :param parameters_set:
        :param z_drift_conf: The current configuration of the positions of the
                       particles.
        :return: A tuple with the local energy contributions.
        """
        kinetic_energy = 0.
        potential_energy = 0.

        # Stop summing contributions from one-body terms when the
        # external potential is small enough. This condition corresponds
        # to a uniform Bose gas.
        if gas_is_free and gas_is_ideal:
            return 0., 0.

        # Add the contributions from all the particles.
        for idx in range(nb):
            particle_energy_parts = particle_local_energy_parts(
                idx, parameters_set, z_drift_conf
            )
            kinetic_energy += particle_energy_parts[0]
            potential_energy += particle_energy_parts[1]

        return kinetic_energy, potential_energy

    @jit(nopython=True)
    def local_energy(parameters_set, z_drift_conf):
        """Computes the local energy for a given configuration of the
        position of the bodies.

        :param parameters_set:
        :param z_drift_conf:
        """
        total_energy = 0.
        for energy in local_energy_parts(parameters_set, z_drift_conf):
            total_energy += energy

        return total_energy

    @jit(nopython=True)
    def local_energy_to_buffer(parameters_set, z_drift_conf, result):
        """

        :param parameters_set:
        :param z_drift_conf:
        """
        result[0] = local_energy(parameters_set, z_drift_conf)

    @jit(nopython=True)
    def local_one_body_density(parameters_set, z_drift_conf, result):
        """Computes the logarithm of the local one-body density matrix
        for a given configuration of the position of the bodies and for a
        specified particle index.

        :param parameters_set:
        :param wf_params:
        :param z_drift_conf:
        :param result:
        :return:
        """

        #
        wf_params, density_params = parameters_set

        # The tuple of physical parameters to evaluate the one-body local
        # density.
        z_rel, = density_params

        # The variational parameters ``rm, k2, beta, r_off, am``.
        rm, rl, k2, beta, r_off, am = wf_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        if gas_is_free and gas_is_ideal:
            result[0] = 1.
            return

        # The average natural logarithm of the density.
        local_density = 0.

        # The local one-body density matrix is calculated as the quotient
        # of the wave function with the ``idx`` particle shifted a distance
        # ``z_rel`` from its original position divided by the wave function
        # with the particles evaluated in their original positions. To improve
        # statistics, we average over all possible particle displacements.
        for idx in range(nb):

            # The natural logarithm of the density.
            local_density_idx_log = 0.

            if not gas_is_free:

                # Position in units of potential period.
                z_period_idx = z_drift_conf[idx, 0] * rl

                # Relative distance in units of potential period.
                z_period_rel = z_rel * rl

                one_body_amp = sqrt(1 + u0 / e0 * sinh(
                    0.5 * kp1 * r / (1 + r)
                ) ** 2.0)

                z_period_idx_cell = z_period_idx % 1
                if 1 / (1 + r) < z_period_idx_cell:
                    one_body_fn_no_shift = cosh(kp1 * (
                        z_period_idx_cell - 1 + 0.5 * r / (1 + r)
                    ))
                else:
                    one_body_fn_no_shift = one_body_amp * cos(
                        k1 * (z_period_idx_cell - 0.5 / (1 + r))
                    )

                z_idx_shift_left = (z_period_idx + z_period_rel) % 1
                if 1 / (1 + r) < z_idx_shift_left:
                    one_body_fn_shift_left = cosh(kp1 * (
                        z_idx_shift_left - 1 + 0.5 * r / (1 + r)
                    ))
                else:
                    one_body_fn_shift_left = one_body_amp * cos(
                        k1 * (z_idx_shift_left - 0.5 / (1 + r))
                    )

                # Accumulate terms.
                local_density_idx_log += (
                    log(one_body_fn_shift_left) - log(one_body_fn_no_shift)
                )

            if not gas_is_ideal:

                # Position in units of simulation box size.
                z_idx = z_drift_conf[idx, 0]

                # Term with the ``idx`` particle shifted left.
                # Relocate the particle within the unitary cell.
                z_idx_shift_left = (z_idx + z_rel) % 1

                for jdx in range(nb):
                    if idx == jdx:
                        continue

                    z_jdx = z_drift_conf[jdx, 0]

                    # Term with no shift
                    r_ij = fabs(z_idx - z_jdx)
                    if r_ij > 0.5:
                        # two_body_fn_no_shift = 1.
                        r_ij = 1 - r_ij

                    if r_ij < fabs(rm):
                        # Two-body term.
                        two_body_fn_no_shift = (
                            am * cos(k2 * (r_ij - r_off))
                        )

                    else:
                        # Two-body term.
                        two_body_fn_no_shift = (sin(pi * r_ij) ** beta)

                    r_ij = fabs(z_idx_shift_left - z_jdx)
                    if r_ij > 0.5:
                        # two_body_fn_shift_left = 1.
                        r_ij = 1 - r_ij

                    if r_ij < fabs(rm):
                        # Two-body term.
                        two_body_fn_shift_left = (
                            am * cos(k2 * (r_ij - r_off))
                        )

                    else:
                        two_body_fn_shift_left = (sin(pi * r_ij) ** beta)

                    # Accumulate terms.
                    local_density_idx_log += (
                        log(two_body_fn_shift_left) - log(two_body_fn_no_shift)
                    )

            # Accumulate total.
            local_density += exp(local_density_idx_log)

        result[0] = local_density / nb

    @jit(nopython=True)
    def local_two_body_correlation(parameters_set, z_drift_conf, result):
        """Computes the local two-body correlation function for a given
        configuration of the position of the bodies.

        :param parameters_set:
        :param z_drift_conf:
        :param result:
        :return:
        """

        # Unpack the parameters.
        _, __, ___, correlation_params = parameters_set

        # The tuple of physical parameters to evaluate the two-body
        # correlation distribution.
        left_bins, right_bins = correlation_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        # if gas_is_free and gas_is_ideal:
        #     result[:] = 1.
        #     return

        # Reset the count.
        result[:] = 0.

        for idx in range(nb):

            z_idx = z_drift_conf[idx, 0] % 1
            z_idx_bin = int(z_idx * left_bins)

            for jdx in range(idx + 1, nb):
                z_jdx = z_drift_conf[jdx, 0] % 1
                z_jdx_bin = int(z_jdx * right_bins)

                # Accumulate the count in the bin that corresponds to both
                # the left and right positions.
                result[z_idx_bin, z_jdx_bin, 0] += 1

    @jit(nopython=True)
    def local_structure_factor(parameters_set, z_drift_conf, result):
        """Computes the local two-body correlation function for a given
        configuration of the position of the bodies.

        :param parameters_set:
        :param z_drift_conf:
        :param result:
        :return:
        """

        # Unpack the parameters.
        physical_parameters, __, ___, ____ = parameters_set

        # The tuple of physical parameters to evaluate the two-body
        # correlation distribution.
        nkz, = physical_parameters

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        # if gas_is_free and gas_is_ideal:
        #     result[:] = 1.
        #     return

        for ndx in range(1, nkz):
            # Initial contributions for the ``ndx`` momentum.
            sum_sin = 0
            sum_cos = 0

            k_ndx = 2 * pi * ndx
            # This is the value of the momentum. As we have a periodic system
            # we are constrained to momenta whose values are integer multiple
            # of 2 * pi (the simulation box size being the unit).

            for idx in range(nb):
                z_idx = z_drift_conf[idx, 0]

                # Accumulate both contributions to the structure factor.
                sum_cos += cos(k_ndx * z_idx)
                sum_sin += sin(k_ndx * z_idx)

            result[ndx, 0] = sum_cos ** 2 + sum_sin ** 2

    return OrderedDict({
        'WAVE_FUNCTION': wave_function,
        'WAVE_FUNCTION_LOG': wave_function_log,
        'LOCAL_ENERGY': local_energy,
        'LOCAL_ENERGY_TO_BUFFER': local_energy_to_buffer,
        'LOCAL_ONE_BODY_DENSITY': local_one_body_density,
        'LOCAL_TWO_BODY_CORRELATION': local_two_body_correlation,
        'LOCAL_STRUCTURE_FACTOR': local_structure_factor
    })


class PhononModel(LiebModel):
    """Represents a many-body quantum system whose trial wave function is
    built from a product of two-body trial functions of phonon type.

    :param potential_magnitude: The potential magnitude in dimension-less
                                units.
    :param geometric_ratio: The ratio width/separation of the barriers,
                            i.e. :math:`b/a`.
    :param interaction_strength: The interaction potential strength
                                 :math:`\gamma`
    :param particles_number: The density of bosons within an unit cell of the
                             system.
    """

    def __init__(self, potential_magnitude, geometric_ratio,
                 interaction_strength, particles_number,
                 evaluate_drift=False):
        super(PhononModel, self).__init__(potential_magnitude,
                                          geometric_ratio,
                                          interaction_strength,
                                          particles_number,
                                          evaluate_drift)

    def compile_core(self, evaluate_drift):
        """Compiles the performance-critical routines.

        :return: A dictionary with the compiled routines.
        """
        return _compiled_model(*self.params, evaluate_drift=evaluate_drift)

    @staticmethod
    def two_body_parameters(gamma, boson_number, rm):
        """Calculate the unknown constants that join the two pieces of the
        two-body functions of the Jastrow trial function at the point `zm_var`.
        The parameters are a function of the boson interaction magnitude `g`
        and the average linear density `boson_number` of the system.

        :param gamma: The magnitude of the interaction between bosons.
        :param boson_number: The density of bosons in the simulation box.
        :param rm: The point where both the pieces of the function must be
                   joined.
        :return: The two body momentum that match the functions.
        """
        if gamma == 0:
            return 0, 0, 1 / 2, 1

        def _nonlinear_equation(k2rm, *args):
            a1d = args[0]
            beta_rm = tan(pi * rm) / pi if k2rm == 0 else (
                k2rm / pi * (rm - k2rm * a1d * tan(k2rm)) * tan(pi * rm) /
                (k2rm * a1d + rm * tan(k2rm))
            )

            # Equality of the local energy at `rm`.
            fn2d_rm_eq = (
                (k2rm * sin(pi * rm)) ** 2 +
                (pi * beta_rm * cos(pi * rm)) ** 2 -
                pi ** 2 * beta_rm * rm
            )

            return fn2d_rm_eq

        # The one-dimensional scattering length.
        # ❗ NOTICE ❗: Here has to appear a two factor in order to be
        # consistent with the Lieb-Liniger theory.
        a1d = 2.0 / (gamma * boson_number)

        k2rm = brentq(_nonlinear_equation, 0, pi / 2, args=(a1d,))

        beta_rm = (
            k2rm / pi * (rm - k2rm * a1d * tan(k2rm)) * tan(pi * rm) /
            (k2rm * a1d + rm * tan(k2rm))
        )

        k2 = k2rm / rm
        k2r_off = atan(1 / (k2 * a1d))

        beta = beta_rm / rm
        r_off = k2r_off / k2
        am = sin(pi * rm) ** beta / cos(k2rm - k2r_off)

        # The coefficient `am` is fixed by the rest of the parameters.
        # am = sin(pi * rm) ** beta / cos(k2 * (rm - r_off))

        return k2, beta, r_off, am

    def subsidiary_params(self, rm: float) -> tuple:
        """Return the params that join the two-body piecewise function.

        :param rm: The point where both the pieces of the function must be
                   joined.
        :return:
        """
        g = self.params[2]
        nb = self.params[3]
        return self.two_body_parameters(g, nb, rm)


two_body_parameters = PhononModel.two_body_parameters


class DriftModel(PhononModel):
    """Represents a many-body quantum system whose trial wave function is
    built from a product of two-body trial functions of Lieb-Liniger type.
    In addition, the core functions are compiled so the drift velocity
    (the gradient of the wave function) is evaluated for the current
    configuration of the system. This task is not realized by the core
    functions of the :class:`LiebModel` as it is a relatively costly
    operation.
    """

    def compile_core(self, evaluate_drift):
        # Enable the evaluation of the drift velocity during compilation.
        return _compiled_model(*self.params, evaluate_drift=True)
