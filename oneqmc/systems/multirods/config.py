from oneqmc.config import ConfigSet
from oneqmc.defaults import CORRELATION_KEY, DENSITY_KEY, META_KEY, \
    METROPOLIS_KEY, STRUCTURE_FACTOR_KEY, SYSTEM_KEY, evolution_defaults, \
    metropolis_diffusive_defaults, metropolis_uniform_defaults

__all__ = ['UniformVMCSchema', 'DiffusiveVMCSchema', 'variational_schema']

#
meta_defaults = {
    'name': 'Multi-Rods_Simulation',
    'description': 'QMC simulation for a multi-rods system',
    'tags': ['Multi-Rods'],
    'type': 'Variational'
}

#
system_defaults = {
    'potential_magnitude': 0.,
    'geometric_ratio': 1.,
    'interaction_strength': 2.,
    'particles_number': 1
}

variational_defaults = {
    'one_body_fn': {
        'box_period_ratio': 1.
    },
    'two_body_fn': {
        'match_point': 0.1,
    }
}

#
density_defaults = {
    'relative_distance': 0.25
}

#
correlation_defaults = {
    'left_bins': 100,
    'right_bins': 100
}

#
structure_factor_defaults = {
    'momenta_number': 100
}

# Schemas are simply instances of configuration sets.
# These variables are intended to be used for identification
meta_schema = ConfigSet(meta_defaults)
system_schema = ConfigSet(system_defaults)
variational_schema = ConfigSet(variational_defaults)
density_schema = ConfigSet(density_defaults)
correlation_schema = ConfigSet(correlation_defaults)
structure_factor_schema = ConfigSet(structure_factor_defaults)
metropolis_uniform_schema = ConfigSet(metropolis_uniform_defaults)
metropolis_diffusive_schema = ConfigSet(metropolis_diffusive_defaults)
evolution_schema = ConfigSet(evolution_defaults)


class UniformVMCSchema(ConfigSet):
    """A configuration set with the common fields for a Variational
    Monte Carlo execution for a multi-rods. This schema corresponds to
    a Metropolis-Hastings execution with uniform, random displacements.

    :param name_: A name of the schema.
    """
    __slots__ = ()

    __schema__ = dict([
        (META_KEY, meta_defaults),
        (SYSTEM_KEY, system_defaults),
        (DENSITY_KEY, density_defaults),
        (CORRELATION_KEY, correlation_defaults),
        (STRUCTURE_FACTOR_KEY, structure_factor_defaults),
        (METROPOLIS_KEY, metropolis_uniform_defaults)
    ])

    def __init__(self, name_=None):
        super().__init__(self.__schema__, name_=name_)


uniform_vmc_schema = UniformVMCSchema()


class DiffusiveVMCSchema(UniformVMCSchema):
    """A configuration set with the common fields for a Variational
        Monte Carlo execution for a multi-rods. This schema corresponds to
        a Metropolis-Hastings execution with gaussian, isotropic, random
        displacements plus with a drift term.
        """
    __slots__ = ()

    __schema__ = dict([
        (META_KEY, meta_defaults),
        (SYSTEM_KEY, system_defaults),
        (DENSITY_KEY, density_defaults),
        (CORRELATION_KEY, correlation_defaults),
        (STRUCTURE_FACTOR_KEY, structure_factor_defaults),
        (METROPOLIS_KEY, metropolis_diffusive_defaults)
    ])


diffusive_vmc_schema = DiffusiveVMCSchema()
