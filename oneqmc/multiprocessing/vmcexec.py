from collections import Iterator, Mapping
from datetime import datetime
from multiprocessing.pool import Pool

import numpy as np

from oneqmc.config import ConfigSet
from oneqmc.defaults import META_KEY, meta_defaults, grid_config_defaults
from oneqmc.progress import ProgressBar
from oneqmc.simulation import Estimator, Grid, GridSpec


class ConfigGrid(Iterator):
    """"""

    __exec_config__ = {
        META_KEY: meta_defaults,
        'estimator': None,
        'grid_spec': grid_config_defaults
    }

    def __init__(self, eval_config: Mapping):
        """Initializes a :class:`ConfigGrid` instance from a mapping 
        object.
        
        :param eval_config: The mapping object that has the configuration of
                            the grid.
        """
        eval_config = ConfigSet(eval_config)

        # TODO: Handle case without major or minor keys.

        elements = eval_config['grid_elements']
        major_grid_spec = GridSpec(elements['major_grid'], eval_config)
        if elements.get('minor_grid') is None:
            minor_grid_spec = None
        else:
            minor_grid_spec = GridSpec(elements['minor_grid'], eval_config)

        self.eval_config = eval_config
        self.grid = Grid(major_grid_spec)
        self.grid_size = self.grid.grid_size
        self.minor_grid_spec = minor_grid_spec

    def __iter__(self):
        return self

    def __next__(self):
        #
        grid_item = next(self.grid)

        # TODO: check memory consumption using plain dicts.
        item_config = ConfigSet(self.eval_config)
        item_config.update(self.eval_config)
        for idx, (name, value) in enumerate(grid_item):
            item_config[name] = value

        # FIXME: We may need a plain ``dict`` to avoid pickling errors.
        # item_config = dict(item_config)

        if self.minor_grid_spec is not None:
            # Items in the ``sub_grid_iter`` object will be executed
            # always in a single process, never distributed among
            # processes in a pool.
            grid_elements = item_config['grid_elements']
            grid_elements['major_grid'] = self.eval_config[
                'grid_elements']['minor_grid']
            grid_elements['minor_grid'] = None
            return ConfigGrid(item_config)

        # Items submitted from the ``grid`` iterator will serve
        # to create a new instance of the estimator of interest. A single
        # estimator instance may be used in case that this iterator
        # is used in a single process.
        return item_config


class MapperKernel(object):
    """"""

    def __init__(self, type_or_inst, callback=None):
        """

        :param type_or_inst: 
        :param callback: 
        """
        if isinstance(type_or_inst, Estimator):
            make_new_on_call = False
        elif issubclass(type_or_inst, Estimator):
            make_new_on_call = True
        else:
            raise ValueError('instance and instance are None')

        self.new_on_call = make_new_on_call
        self.type_or_inst = type_or_inst
        self.callback = callback

    @classmethod
    def from_type(cls, *args, **kwargs):
        """
        
        :param args: 
        :param kwargs: 
        :return: 
        """
        return cls(None, *args, **kwargs)

    def __call__(self, config_obj):
        """Callable implementation.
        
        :param config_obj: 
        :return: 
        """
        if isinstance(config_obj, ConfigGrid):
            # Items in the ``config_obj`` object will be executed
            # always in a single process, never distributed among
            # processes in a pool.
            mapper = SMPMapper(self.type_or_inst, callback=self.callback)
            result = mapper.sequential_exec(config_obj)
            return result

        # Items submitted from the ``config_obj`` iterator will serve
        # to create a new instance of the estimator of interest. A single
        # estimator instance may be used in case that this iterator
        # is used in a single process.
        if self.new_on_call:
            instance = self.type_or_inst(config_obj)
            result = instance.eval(config_obj)
        else:
            result = self.type_or_inst.eval(config_obj)

        if self.callback is not None:
            result = self.callback(result)

        return result


class SMPMapper(object):
    """
    """

    __exec_config__ = {
        META_KEY: meta_defaults,
        'estimator': None,
        'grid_spec': grid_config_defaults
    }

    #:
    def __init__(self, type_or_inst: Estimator or type(Estimator),
                 force_new=False,
                 processes=None,
                 callback=None
                 ):
        """Initializes a :class:`SMPMapper` instance.
        
        :param type_or_inst: 
        :param force_new: 
        :param processes: 
        :param callback: 
        """
        #:
        self.type_or_inst = type_or_inst

        #:
        self.force_new = force_new

        #:
        self.processes = processes

        #:
        self.callback = callback

    def has_instance_param(self, grid_spec):
        """

        :param grid_spec:
        :return:
        """
        has_instance_param = False
        for _, item in grid_spec.items():
            for name in item.names:
                if self.type_or_inst.is_instance_param(name):
                    has_instance_param = True
                    break
        return has_instance_param

    def get_type_or_instance(self, config_grid, force_new=False):
        """
        
        :param config_grid: 
        :param force_new: 
        :return: 
        """
        eval_config = config_grid.eval_config
        major_grid_spec = config_grid.grid.grid_spec
        minor_grid_spec = config_grid.minor_grid_spec

        make_new_in_kernel = self.has_instance_param(major_grid_spec)
        if minor_grid_spec is not None:
            if self.has_instance_param(minor_grid_spec):
                make_new_in_kernel = True

        type_or_inst = self.type_or_inst
        if isinstance(type_or_inst, Estimator):
            instance = type_or_inst
            type_ = type(instance)
        elif issubclass(type_or_inst, Estimator):
            instance = None
            type_ = type_or_inst
        else:
            raise ValueError('')

        if force_new or make_new_in_kernel:
            return type_
        if instance is not None:
            return instance
        return type_(eval_config)

    def sequential_exec(self, config_grid):
        """"""

        exec_result = []
        progress_bar = ProgressBar(max_value=config_grid.grid_size)

        type_or_inst = self.get_type_or_instance(config_grid, self.force_new)
        kernel = MapperKernel(type_or_inst, self.callback)

        for idx, config_item in enumerate(config_grid):
            result = kernel(config_item)
            exec_result.append(result)
            progress_bar.update(idx + 1)

        progress_bar.finish()
        exec_result = np.array(exec_result)
        return exec_result

    def exec(self, config_grid):
        """"""

        exec_result = []
        exec_start = datetime.now()
        progress_bar = ProgressBar(max_value=config_grid.grid_size)

        type_or_inst = self.get_type_or_instance(config_grid, self.force_new)
        kernel = MapperKernel(type_or_inst, self.callback)

        with Pool(processes=self.processes) as pool:
            # Enumerate the generator in order to track the progress.
            pool_imap = pool.imap(kernel, config_grid)
            for idx, process_result in enumerate(pool_imap):
                exec_result.append(process_result)
                progress_bar.update(idx + 1)

        progress_bar.finish()
        exec_finish = datetime.now()
        exec_result = np.array(exec_result)
        return exec_result, (exec_finish, exec_start)
