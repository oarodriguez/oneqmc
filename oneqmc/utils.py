import functools
import inspect
import os
import warnings
from datetime import datetime
from functools import reduce
from socket import gethostname

import numpy as np
from tzlocal import get_localzone


# NOTE (kgriffs): We don't want our deprecations to be ignored by default,
# so create our own type.

class DeprecatedWarning(UserWarning):
    """Copyright (c), Kurt Griffiths
    https://gist.github.com/kgriffs/8202106
    """
    pass


def deprecated(instructions):
    """Flags a method as deprecated.

    :param instructions: A human-friendly string of instructions, such
                         as: 'Please migrate to add_proxy() ASAP.'

    Copyright (c), Kurt Griffiths
    https://gist.github.com/kgriffs/8202106
    """

    def decorator(func):
        """This is a decorator which can be used to mark functions
        as deprecated. It will result in a warning being emitted
        when the function is used."""

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            message = 'Call to deprecated function {}.\n{}'.format(
                func.__name__, instructions)

            frame = inspect.currentframe().f_back

            warnings.warn_explicit(message,
                                   category=DeprecatedWarning,
                                   filename=inspect.getfile(frame.f_code),
                                   lineno=frame.f_lineno)

            return func(*args, **kwargs)

        return wrapper

    return decorator


def now_to_string(fmt, local_now=True):
    """Returns the current date and time as a string in the specified
    format.

    :param fmt: The format string to pass to the ``strftime`` datetime
                object method.
    :param local_now: Indicates if the current time should be determined
                      with respect to the current OS timezone. Defaults to
                      ``True``. The OS timezone is obtained from
                      ``get_localzone`` function from the package ``tzlocal``.
    :return: The string representation of the datetime.
    """
    local_tz = None if local_now is None else get_localzone()
    _now = datetime.now(local_tz)
    return _now.strftime(fmt)


def date_string(add_date=True, add_time=True, add_ms=False, add_tz=True,
                bracket=True):
    """Creates a string which includes the current date and can optionally
    include time, current microseconds and timezone.

    :param add_date: Attach the current date to the string. Defaults to
                    ``True``.
    :param add_time: Attach the current time to the string. Defaults to
                    ``True``.
    :param add_ms: Attach the current microseconds to the string. Defaults
                   to ``False``.
    :param add_tz: Attach the local timezone information to the string.
                   Defaults to ``True``.
    :param bracket: If ``True`` it surrounds the date string with square
                    brackets, ``[date_string]``. Defaults to ``True``.
    :return: The string with date, time and timezone.
    """
    part_fmt = '[{:s}]' if bracket else '{:s}'
    date_fmt_parts = []

    if not (add_date or add_time or add_tz):
        raise ValueError(
            'Date string must have at least date, time or timezone '
            'information.'
        )

    if add_date:
        date_fmt_parts.append('%Y%m%d')

    if add_time and add_ms:
        date_fmt_parts.append('%H%M-%Ss.%fus')
    elif add_time:
        date_fmt_parts.append('%H%M-%Ss')

    if add_tz:
        date_fmt_parts.append('%Z%z')

    date_fmt = part_fmt.format('_'.join(date_fmt_parts))
    formatted_string = now_to_string(fmt=date_fmt)
    return formatted_string


def tag_date_string(tags, bracket=True, **date_options):
    """Creates a string with a list of tags embedded in addition to a dated
    string generated by :func:`date_string`.

    :param tags: A list of identifiers ``[id1, id2]`` to append to the
                 filename in the format ``#id1#id2``. Defaults to ``None``.
    :param bracket: If ``True`` it surrounds the tag string and the date
                    string with square brackets. Defaults to ``True``.
    :param date_options: Keyword arguments to be passed to
                         :func:`date_string`.
    :return: A string with tags and (optionally) date embedded.
    """
    part_fmt = '[{:s}]' if bracket else '{:s}'
    parts = []

    tag_fmt = '#{:s}'
    tags_parts = []
    for tag in tags:
        tags_parts.append(tag_fmt.format(str(tag)))

    if len(tags_parts) > 0:
        tags_string = part_fmt.format(''.join(tags_parts))
        parts.append(tags_string)

    parts.append(date_string(bracket=bracket, **date_options))

    return ''.join(parts)


def tag_date_name(base, tags, ext=None, add_hostname=False, bracket=True,
                  **date_options):
    """Generates a string intended to be used as a filename with a list fo
    tags embedded. Neither the ``base`` string or the elements in the ``tags``
    argument are checked out for the presence of file's path invalid
    characters in the current OS.

    :param base: The base string for the filename.
    :param tags: A list of identifiers ``[id1, id2]`` to append to the
                 filename in the format ``#id1#id2``. Defaults to ``None``.
    :param ext: The extension of the filename. Defaults to ``'txt'``.
    :param add_hostname: Whether or not to embed the current computer hostname
                         to the string. This is recommended if we want to
                         identify a computer by just watching the filename.
                         Defaults to ``False``.
    :param bracket: If ``True`` it surrounds the name string, the tags
                    string and the date string with square brackets. Defaults
                    to ``True``.
    :param date_options: Keyword arguments to be passed to
                         :func:`tag_date_string`.
    :return: A string intended to be used as filename.
    """
    part_fmt = '[{:s}]' if bracket else '{:s}'
    name_fmt = '{:s}.{:s}' if ext is not None else '{:s}'
    tags_list = tags or []
    name = ''.join(
        [base, tag_date_string(tags_list, bracket=bracket, **date_options)]
    )

    if add_hostname:
        name += part_fmt.format('@' + gethostname())

    return name_fmt.format(name, ext)


def flatten_array(data):
    """Takes the supplied ``array`` or ``list`` object from the ``data``
    argument and flatten it so it can be used for parallel computations.

    :param data: The array to flatten.
    :return: The flattened array.
    """
    if not isinstance(data, np.ndarray):
        flat_data = np.array(data)
    else:
        # TODO: Should we use just the original data?
        flat_data = data.copy()
    data_shape = flat_data.shape
    shape_to_map = (
        reduce(lambda s1, s2: s1 * s2, data_shape[:-1]),
        data_shape[-1]
    )
    flat_data.reshape(shape_to_map)
    return flat_data


def flat_option_dict_generator(keys, options_values, size):
    """Returns an object that generates a sequence of dictionaries with
     options. The resulting object is suitable to be used for parallel
     computations

    :param keys: The keys used to build each option dictionary.
    :param options_values: The values to build the option dictionary.
    :param size: The number of option dictionaries.
    :return: A generator of option dictionaries.
    """

    def option_dict(values):
        #: Assume Mappable and return it.
        if hasattr(values, 'keys'):
            return values
        return dict(zip(keys, values))

    #: If ``options_values`` is a single element of Mapping type,
    #: yield its value as many times as indicated by ``size``.
    if hasattr(options_values, 'keys'):
        for idx in range(size):
            yield options_values
        return

    # If ``option_values`` is a Numpy array or some sequence type that can be
    # converted transparently to a numpy array of ``float64`` elements, just
    # let's use it.
    if isinstance(options_values, np.ndarray):
        options_array = options_values
    else:
        options_array = np.array(options_values, dtype=np.float64)

    if len(options_array.shape) != 2:
        raise ValueError(
            "Options must be a two-dimensional array."
        )

    shape_to_map = (size, options_array.shape[-1])
    for values in options_array.reshape(shape_to_map):
        yield option_dict(values)


def make_date_dirs(date: datetime, bases: tuple or list = None,
                   prefix: str = None) -> str:
    """

    :param date:
    :param bases:
    :param prefix:
    :return:
    """
    prefix = prefix or '.'
    bases = bases or []
    year, month, day = date.strftime('%Y>>%m>>%d').split('>>')
    path_elements = bases + [year, month, day]
    sub_path = os.path.join(*path_elements)
    os.makedirs(sub_path, exist_ok=True)
    return os.path.join(prefix, sub_path)
