"""
    oneqmc.storage
    ~~~~~~~~~~~~~~

    This module contains routines to read and write HDF5 files to save data
    from Quantum Monte Carlo simulations.

    :copyright: (c) 2016 by Abel Rodríguez.
"""

from h5py import File, Group

from oneqmc.utils import date_string, tag_date_name

#: Some important constants.
DESCRIPTION_KEY = 'Description'
CREATION_DATE_KEY = 'Creation Date'
INCLUSION_DATE_KEY = 'Inclusion Date'

#: The default value of the `description` attribute to add to every created
#: Quantum Monte Carlo file.
QMC_DESCRIPTION = 'HDF5 file to store data that belongs to a Quantum Monte ' \
                  'Carlo simulation'

#: The name of the base group of Variational Monte Carlo data.
VMC_GROUP = 'VMC'

#: The default value of the `description` attribute to add to a created
#: Variational Monte Carlo group.
VMC_GROUP_DESCRIPTION = 'Group for data corresponding to a Variational ' \
                        'Monte Carlo simulation.'

#: The default value of the `description` attribute to add to a Variational
#: Monte Carlo dataset.
VMC_DATASET_DESCRIPTION = 'Dataset for data corresponding to a Variational ' \
                          'Monte Carlo simulation.'


def set_qmc_description(h5file: File, description=None):
    """Adds a description to an HDF5 file used to store QMC data.

    :param h5file: The `h5py.File` object.
    :param description: The description to add. If set to ``None``, a
                        default description will be added.
    :return:
    """
    h5file.attrs.update([
        (DESCRIPTION_KEY, description or QMC_DESCRIPTION)
    ])


def create_vmc_group(h5file: File, description=None):
    """Creates a subgroup within the base VMC group. Does not handle
    exceptions if the group already exists.

    :param h5file: The `h5py.File` instance where the VMC group will
                   be created.
    :param description: The description to add. If set to ``None``, a
                        default description will be added.
    :return: The newly created `h5py.Group` instance.
    """
    base_vmc_group = h5file.create_group(VMC_GROUP)

    #: Set its description.
    base_vmc_group.attrs.update([
        (DESCRIPTION_KEY, description or VMC_GROUP_DESCRIPTION)
    ])
    return base_vmc_group


def create_vmc_dataset(group: Group, name, description=None, *args, **kwargs):
    """Creates a new dataset to store VMC relative data.

    :param group: The `h5py.Group` where the dataset will be created in.
    :param name: The name of the dataset.
    :param description: A description for the new dataset. Defaults to `None`.
                        If it is not specified, the value of
                        :const:`VMC_DATASET_DESCRIPTION` will be used.
    :param args: Arguments to pass to `h5py.File.create_dataset` method.
    :param kwargs: Keyword arguments to pass to `h5py.File.create_dataset`
                   method.
    :return: The newly created dataset.
    """
    dataset = group.create_dataset(name, *args, **kwargs)
    inclusion_date = date_string()
    dataset.attrs.update([
        (INCLUSION_DATE_KEY, inclusion_date),
        (DESCRIPTION_KEY, description or VMC_DATASET_DESCRIPTION)
    ])
    return dataset


def qmc_filename(base='QMCData', tags=None, ext='h5', **filename_options):
    """Generates a string intended to be used as name for a file to save data
    from a Quantum Monte Carlo simulation.

    :param base:
    :param tags: A list of identifiers ``[id1, id2, ...]`` to append to the
                 filename in the format ``#id1#id2``. Defaults to ``None``.
    :param ext: The file extension. Defaults to ``'h5'``.
    :param filename_options: A dictionary of keyword arguments to be passed
                             to :func:`tag_date_filename`.
    :return: The string to be used as a filename.
    """
    tag_list = tags or []
    return tag_date_name(base, tag_list, ext=ext, **filename_options)
