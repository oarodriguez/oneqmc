`OneQMC`_
=========

A library to perform Quantum Monte Carlo simulations for many-body quantum
systems. It's written in pure Python, as it uses `Numba`_ for
performance-critical functions that execute CPU-intensive calculations.

The library is released under the BSD-2 License.

Authors
-------

**Omar Abel Rodríguez López**

- Github profile: `https://github.com/oarodriguez <https://github.com/oarodriguez>`_
- Bitbucket profile: `https://bitbucket.org/oarodriguez <https://bitbucket.org/oarodriguez>`_

.. _OneQMC: https://bitbucket.org/oarodriguez/oneqmc
.. _Numba: http://numba.pydata.org/
