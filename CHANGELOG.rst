Changelog
=========

This file lists the major changes in OneQMC releases.

Version 0.1
-----------

**First release**.
    Released on May 1st 2016

- ``core.py``: contains routines to perform Quantum Monte Carlo integrations and other basic stuff.
- ``liebliniger.py``: implements VMC for the ground state energy :math:`\langle E \rangle` with Lieb-Liniger trial wave functions.
