from oneqmc.types import DeepFixedDict, FixedDict

JSON_DEFAULT_MOCK = """{
    "name": "Simulation to find two-body correlation distribution",
    "tags": [
        "#Correlations",
        "#Phonon"
    ],
    "type": "Phonon",
    "hamiltonian": {
        "potential_magnitude": 0,
        "geometric_ratio": 1.0,
        "interaction_strength": 1e-3,
        "number_of_bosons": 5
    },
    "variational": {
        "match_point": 0.1,
        "simulation_box_ratio": 1.0
    },
    "simulation": {
        "moves": ["__linspace__", [0, 10, 100]],
        "transient_moves": 1,
        "stabilization_moves": 100,
        "move_amplitude": 1e-2,
        "tolerance": 1e-2,
        "keep_markov_chain": false,
        "processes": null
    }
}
"""

# Keys to be used as configuration values.

NAME = 'Python VMC Simulation'
DESCRIPTION = 'A nice VMC simulation'
TYPE = 'Phonon'
TAGS = ['#Correlations', '#Phonon']

HAMILTONIAN = {
    'potential_magnitude': 0,
    'geometric_ratio': 1.,
    'interaction_strength': 1.,
    'number_of_bosons': 20
}

VARIATIONAL = {
    'match_point': 0.1,
    'simulation_box_ratio': 1.
}

SIMULATION = {
    'moves': 50000,
    'transient_moves': 50,
    'stabilization_moves': None,
    'move_amplitude': 1e-2,
    'keep_markov_chain': False,
    'processes': None,
    'ranges': None
}


def test_fixed_dict_creation():
    dc = {'hamiltonian': HAMILTONIAN}

    assert FixedDict(**dc)
    assert FixedDict(dc)
    assert FixedDict(dc.items(), **dc)

    assert DeepFixedDict(**dc)
    assert DeepFixedDict(dc)
    assert DeepFixedDict(dc.items(), **dc)


def test_fixed_dict_change():
    fixed_dict = FixedDict(HAMILTONIAN)
    deep_fixed_dict = DeepFixedDict(HAMILTONIAN)

    try:
        fixed_dict['moves'] = 1000
        deep_fixed_dict['moves'] = 1000
    except KeyError as e:
        print("Ok, can not add new keys")

    try:
        del fixed_dict['interaction_strength']
        del deep_fixed_dict['interaction_strength']
    except KeyError as e:
        print("Ok, can not delete existing keys")

    # Values are propagating?
    fixed_dict['geometric_ratio'] = 100
    deep_fixed_dict['geometric_ratio'] = 100

    assert fixed_dict['geometric_ratio'] == 100
    assert deep_fixed_dict['geometric_ratio'] == 100


def test_fixed_dict_update():
    u0, r, g1d, n1 = 0., 1., 1., 20
    dc = {
        'hamiltonian': {
            'potential_magnitude': u0,
            'geometric_ratio': r,
            'interaction_strength': g1d,
            'number_of_bosons': n1
        }
    }

    fixed_dict = FixedDict(**dc)

    # Update only the specified keys.
    fixed_dict.update({
        'hamiltonian': {
            'potential_magnitude': 1.2345,
            'interaction_strength': 1.111
        }
    })

    item = fixed_dict['hamiltonian']

    assert item['potential_magnitude'] != u0
    assert item['interaction_strength'] != g1d

    try:
        assert item['geometric_ratio']
        assert item['number_of_bosons']
    except KeyError:
        print("Ok, keys do not exist")


def test_fixed_dict_references():
    dc = {
        'foo': HAMILTONIAN
    }

    fixed_dict = FixedDict(dc)

    # These should point to the same address.
    assert fixed_dict['foo'] is dc['foo']

    dc_foo = fixed_dict['foo']
    # These should point to the same address too.
    assert dc_foo['potential_magnitude'] is dc['foo']['potential_magnitude']

    # This is allowed
    dc['foo'] = None


def test_deep_fixed_dict_update():
    u0, r, g1d, n1 = 0., 1., 1., 20
    dc = {
        'hamiltonian': {
            'potential_magnitude': u0,
            'geometric_ratio': r,
            'interaction_strength': g1d,
            'number_of_bosons': n1
        }
    }

    fixed_dict = DeepFixedDict(**dc)

    # Update only the specified keys.
    fixed_dict.update({
        'hamiltonian': {
            'potential_magnitude': 1.2345,
            'interaction_strength': 1.111
        }
    })

    item = fixed_dict['hamiltonian']

    assert item['potential_magnitude'] != u0
    assert item['interaction_strength'] != g1d
    assert item['geometric_ratio'] == r
    assert item['number_of_bosons'] == n1


def test_deep_fixed_dict_references():
    dc = {
        'foo': HAMILTONIAN
    }

    fixed_dict = DeepFixedDict(dc)

    # These must not point to the same address.
    assert fixed_dict['foo'] is not dc['foo']

    dc_foo = fixed_dict['foo']
    # These should point to the same address too.
    assert dc_foo['potential_magnitude'] is dc['foo']['potential_magnitude']

    # This is not allowed
    try:
        fixed_dict['foo'] = None
    except ValueError:
        print('Nice, key can not replace')
