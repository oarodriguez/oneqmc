from functools import reduce
from operator import mul

import numpy as np

from oneqmc.config import contains_
from oneqmc.multiprocessing.vmcexec import ConfigGrid, SMPMapper
from oneqmc.simulation import Estimator, Grid, GridSpec, GridSpecItem, \
    GSItemType


class MockEstimator(Estimator):
    """An example estimator."""

    __config__ = dict([
        ('foo', True),
        ('bar', 1)
    ])


def test_grid_iterator():
    """Tests the behavior and proper functionality of the iterator
    used by a :class:`Controller` instance to carry on the execution
    of a Quantum Monte Carlo simulation.
    """
    mock_config = {
        'name': 'Mock',
        'foo': np.array([1, 2, 3, 4, 5, 6], dtype='i8'),
        'bar': np.linspace(0., 1., 11),
    }

    grid_spec = GridSpec(['bar'], mock_config)
    grid_iter = Grid(grid_spec)

    for idx, v in enumerate(grid_iter):
        assert v[0][1] == mock_config['bar'][idx]


def test_grid_spec_item():
    """Tests :class:`GridSpecItem` implementation"""

    shp = (2, 3)
    names = ['name1', 'name2']

    # Test ZIP composition
    contents = [
        [[1], [np.pi ** 2]],
        np.random.randint(-100, 100, size=shp)
    ]
    grid_item = GridSpecItem(names, contents, type_=GSItemType.ZIP)
    assert grid_item.names == ('name1', 'name2')
    assert grid_item.shape == (shp[0],)

    # Tests equality of rows.
    v = grid_item[0]
    assert np.array_equal(v[0], contents[0][0])
    assert np.array_equal(v[1], contents[1][0])

    v = grid_item[1]
    assert np.array_equal(v[0], contents[0][1])
    assert np.array_equal(v[1], contents[1][1])

    # Test BROADCAST composition
    grid_item = GridSpecItem(names, contents, type_=GSItemType.BROADCAST)
    assert grid_item.shape == shp


def test_grid_spec():
    """Tests :class:`GridSpecItem` implementation"""

    names = ['name3', {
        'items': ['name1', 'name2'],
        'composition_type': 'BROADCAST'
    }]
    name2_shape = (10, 2, 2, 3)
    contents = {
        'name1': [[1], [np.pi ** 2]],
        'name2': np.random.randint(-100, 100, size=name2_shape),
        'name3': ['Omar', 'Abel']
    }

    grid_spec = GridSpec(names, contents)

    # Test membership
    assert ('name3',) in grid_spec
    assert ('name1', 'name2') in grid_spec

    # Test shapes
    assert grid_spec[('name3',)].shape == (2,)
    assert grid_spec[('name1', 'name2')].shape == name2_shape


class RandomEstimator(Estimator):
    """An example estimator that returns a random number from a Gaussian
    distribution.
    """

    def instance_config(self):
        pass

    __exec_config__ = dict([
        ('mean', 0),
        ('sigma', 1),
        ('amplitude', 1)
    ])

    def eval(self, config):
        config = config
        mean = config['mean']
        sigma = config['sigma']
        amplitude = config['amplitude']
        return amplitude * np.random.normal(mean, scale=sigma)

    @classmethod
    def is_instance_param(cls, config_param):
        return contains_(cls.__exec_config__, config_param)


def cb(c):
    return c ** 1


def test_evaluator():
    """Tests the behavior and proper functionality of the iterator
    used by a :class:`Controller` instance to carry on the execution
    of a Quantum Monte Carlo simulation.
    """
    random_config = {
        'mean': np.random.randint(0, 20, size=(5, 5), dtype='i8'),
        'sigma': np.linspace(1., 5., 1),
        'amplitude': 10.0,

        'grid_elements': dict([
            ('major_grid', [
                {'items': ['mean', 'sigma'], 'composition_type': 'BROADCAST'}
            ]),
            # ('minor_grid', ['sigma'])
        ])
    }

    config_grid = ConfigGrid(random_config)
    mapper = SMPMapper(RandomEstimator, callback=cb)
    exec_result, times = mapper.exec(config_grid)

    mean_ = random_config['mean']
    sigma_ = random_config['sigma']

    # Evaluator result and ``'grid_spec'`` ranges should be consistent.
    broadcast_shape = np.broadcast(mean_, sigma_).shape
    assert reduce(mul, exec_result.shape) == reduce(mul, broadcast_shape)
