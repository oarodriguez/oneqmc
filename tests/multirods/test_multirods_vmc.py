from math import pi

import mpmath as mp
import numpy as np

from oneqmc.systems.multirods.ideal import one_body_eigen_energy


def test_energy_ideal_bose_gas():
    """Tests the energy for an ideal 1D Bose gas within a multi-rods
    structure."""

    # Energy when potential magnitude is nonexistent.
    with mp.extraprec(100):
        r = np.random.randint(0, 1000)
        u0 = 1e6 * np.random.rand()

        assert one_body_eigen_energy(0, r) == 0
        assert one_body_eigen_energy(u0, 0) == 0

    # Checks that the energy of ideal bose gas is lower than the theoretical
    # upper limit even for very large potential magnitudes.
    with mp.extraprec(10000):
        u0 = mp.mpf('1e6')
        r = mp.mpf(10)
        e0 = one_body_eigen_energy(u0, r)
        e0_box = (1 + r) ** 2 * pi ** 2
        assert e0 < e0_box

    # Checks that the energy of the ideal Bose gas goes to zero as the
    # potential magnitude becomes very small.
    with mp.extraprec(100):
        u0 = mp.mpf(1e-6)
        r = mp.mpf(1)
        e0 = one_body_eigen_energy(u0, r)
        assert abs(e0) < 1e-2
