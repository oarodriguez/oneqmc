from oneqmc.systems.multirods import LiebModel

from oneqmc.systems.multirods.config import system_defaults


def test_init_from_spec():
    # FIXME: Make test more interesting :)
    defaults = system_defaults
    mdl = LiebModel(**defaults)

    assert hasattr(mdl, '_compiled_core')
