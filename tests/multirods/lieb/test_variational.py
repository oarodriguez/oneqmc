from oneqmc.config import ConfigSet
from oneqmc.defaults import SYSTEM_KEY
from oneqmc.multiprocessing.vmcexec import ConfigGrid, SMPMapper
from oneqmc.simulation import export
from oneqmc.systems.multirods.ideal import one_body_eigen_energy
from oneqmc.systems.multirods.lieb import EnergyEstimator, OBDMEstimator


def test_energy():
    """Tests the evaluation of the energy for a very dilute gas with very
    small interactions.
    """
    stm_spec = ConfigSet(EnergyEstimator.blank_config_copy())
    system_spec = stm_spec[SYSTEM_KEY]

    # Update to represent a very dilute gas.
    u0 = stm_spec['system.potential_magnitude'] = 100.0
    r = stm_spec['system.geometric_ratio']
    nop = stm_spec['system.particles_number'] = 3

    # A very small interaction strength.
    stm_spec['system.interaction_strength'] = 1e-4

    # Increase random moves.
    stm_spec['metropolis.moves'] = 20000

    stm = EnergyEstimator(stm_spec)
    exec_result = stm.eval()
    energy, error, rate = exec_result.mean_error_rate[0]

    energy_per_particle = energy / nop
    ideal_energy = one_body_eigen_energy(u0, r)

    assert abs(energy_per_particle - ideal_energy) < 1e-2


def callback(result):
    return result.mean_error_rate


def test_energy_from_config_file():
    """Tests the evaluation of the energy for a very dilute gas with very
    small interactions.
    """
    eval_config = ConfigSet({
        'meta': None,
        'estimator': dict(EnergyEstimator.blank_config_copy(),
                          grid_elements=None)
    })
    eval_config.load_json_file('energy.config.json')

    config_grid = ConfigGrid(eval_config['estimator'])
    mapper = SMPMapper(EnergyEstimator, config_grid, callback=callback)

    map_result = mapper.exec()
    stm_result, _ = map_result

    finish_time, start_time = _
    eval_config['meta']['finish_time'] = finish_time
    eval_config['meta']['start_time'] = start_time

    export('./test_export.h5', stm_result, eval_config)

    # First dimension has to coincide.
    assert stm_result.shape[0] == config_grid.grid_size


def test_one_body_density_matrix():
    """Tests the evaluation of the energy for a very dilute gas with very
    small interactions.
    """
    stm_spec = ConfigSet(OBDMEstimator.blank_config_copy())

    # Update to represent a very dilute gas.
    stm_spec['system.potential_magnitude'] = 0.0
    stm_spec['system.particles_number'] = 3

    # A very small interaction strength.
    stm_spec['system.interaction_strength'] = 1e-4

    # Very small relative distance.
    stm_spec['density.relative_distance'] = 1e-1

    # Increase random moves.
    stm_spec['metropolis.moves'] = 20000

    stm = OBDMEstimator(stm_spec)
    exec_result = stm.eval()
    density, error, rate = exec_result.mean_error_rate[0]

    # For the previous configuration values the density matrix must be
    # very close to unity.
    assert abs(density - 1.) < 1e-2
