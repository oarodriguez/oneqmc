import numpy as np

from oneqmc.config import ConfigSet

_JSON_DEFAULT_MOCK = """{
    "execution": {
        "processes": [
            "__arange__", [10, 100, 10], {"dtype": "f8"}
        ],
        "ranges": [
            "simulation.processes"
        ]
    }
}
"""

HAMILTONIAN = {
    'potential_magnitude': 0,
    'geometric_ratio': 1.,
    'interaction_strength': 1.,
    'number_of_bosons': 20
}

VARIATIONAL = {
    'match_point': 0.1,
    'simulation_box_ratio': 1.
}

SIMULATION = {
    'moves': 50000,
    'transient_moves': 50,
    'stabilization_moves': None,
    'move_amplitude': 1e-2,
    'keep_markov_chain': False,
    'processes': None,
    'ranges': None
}

EXECUTION = {
    'processes': np.arange(100, 1000, 100),
    'ranges': ["execution.processes"]
}


class ConfigMock(ConfigSet):
    """Mock class."""
    pass


def test_configurable():
    cfg_defaults = {
        'hamiltonian': HAMILTONIAN,
        'variational': VARIATIONAL,
        'execution': EXECUTION,
        'simulation': SIMULATION
    }
    # Build the configuration directly from an instance.
    cfg_mock = ConfigMock(cfg_defaults)

    # Import from module.
    module_name = __file__.rsplit('.', 1)[0]
    cfg_mock.load_module(module_name)

    # Update from JSON.
    cfg_mock.load_json(_JSON_DEFAULT_MOCK)

    # Checks that transforming the configuration again does not
    # messes up the program.
    cfg_mock.transform_config()

    mock_keys = cfg_mock.config.keys()
    assert 'hamiltonian' in mock_keys
    assert 'variational' in mock_keys
    assert 'execution' in mock_keys


def test_mapping():
    """Tests that dotted-string access for :class:`Set`
    instances works correctly.
    """
    cfg_defaults = {
        'hamiltonian': HAMILTONIAN,
        'variational': {
            'foo': VARIATIONAL
        }
    }
    # Build the configuration directly from an instance.
    cfg_mock = ConfigMock(cfg_defaults)

    nob = HAMILTONIAN['number_of_bosons']
    assert cfg_mock['hamiltonian.number_of_bosons'] == nob
    assert cfg_mock['variational.foo'] == VARIATIONAL

    match_point = VARIATIONAL['match_point']
    assert cfg_mock['variational.foo.match_point'] == match_point


def test_setitem():
    """Tests that dotted-string assignment for :class:`Set`
    instances works correctly.
    """
    cfg_defaults = {
        'hamiltonian': HAMILTONIAN,
        'variational': {
            'foo': VARIATIONAL
        }
    }
    # Build the configuration directly from an instance.
    cfg_mock = ConfigMock(cfg_defaults)

    cfg_mock['hamiltonian.number_of_bosons'] = 100
    cfg_mock['variational.foo.match_point'] = 0.22

    assert cfg_mock['hamiltonian.number_of_bosons'] == 100


def test_contains():
    """Tests that dotted-string membership for :class:`Set`
    instances works well.
    """
    cfg_defaults = {
        'hamiltonian': HAMILTONIAN,
        'variational': {
            'foo': VARIATIONAL
        }
    }
    # Build new configuration.
    cfg_mock = ConfigMock(cfg_defaults)

    assert 'hamiltonian' in cfg_mock
    assert 'variational' in cfg_mock
    assert 'variational.foo' in cfg_mock
    assert 'variational.foo.match_point' in cfg_mock
    assert 'variational.foo.bar' not in cfg_mock
