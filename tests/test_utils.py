from datetime import datetime

from oneqmc.utils import date_string, tag_date_string, tag_date_name


def test_date_string():
    """Tests :func:`date_string`."""
    date_str = date_string(add_time=True, add_ms=True, add_tz=False)
    assert datetime.strptime(date_str, '[%Y%m%d_%H%M-%Ss.%fus]')


def test_tag_date_string():
    """Tests :func:`tag_date_string`."""
    tags = ['VMC', 'PhononType']
    tag_date_str = tag_date_string(tags, add_time=True, add_ms=True,
                                   add_tz=False)
    tag_date_fmt = '[#VMC#PhononType][%Y%m%d_%H%M-%Ss.%fus]'
    assert datetime.strptime(tag_date_str, tag_date_fmt)


def test_tag_date_filename():
    """Tests :func:`tag_date_filename`."""
    base = 'QMCData'
    tags = ['VMC', 'PhononType']
    tag_date_str = tag_date_name(base, tags, add_time=True,
                                 add_ms=True, add_tz=False, ext='h5')
    tag_date_fmt = '{:s}[#VMC#PhononType][%Y%m%d_%H%M-%Ss.%fus].h5'.format(
        base
    )
    assert datetime.strptime(tag_date_str, tag_date_fmt)
