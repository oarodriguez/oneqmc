OneQMC Documentation
====================

OneQMC is a package to perform Monte Carlo simulations over quantum many-body
systems. The goal is to obtain approximations for the energy of the ground
state and other properties of the system.
